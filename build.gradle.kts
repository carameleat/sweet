//                                          Bismillahirrahmanirrahim


buildscript {
    repositories {
        mavenCentral()
    }
    dependencies {
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.6.0")
        classpath("com.github.jengelman.gradle.plugins:shadow:6.1.0")
    }
}

plugins {
    kotlin("jvm") version "1.6.0"
    kotlin("kapt") version "1.6.0"
}

val group_name: String by project
val sweet_version: String by project
group = group_name
version = sweet_version

allprojects {
    repositories {
        mavenCentral()
    }
}