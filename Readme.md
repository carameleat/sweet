#### Bismillahirrahmanirrahim (In the name of Allah, The Entirely Merciful, the Especially Merciful)

# Sweet

## Overview
Sweet is an SQL DSL library that aim to 
simplify on how to use the JDBC API, especially on creating the PreparedStatement.

We do not aim Sweet to be an ORM.

## Supported Database
Right now, there is only MySQL implementation. We will thankfully if you who are familiar with
DBMS other than MySQL could write the implementation for the DBMS that you familiar with.

## How to Use
* ### Set the Writers
Firstly, you need to set the Writers on your start point.
````
import com.carameleat.sweet.writers

fun main(args: Array<String>) {
    Writer.apply {
        InsertWriter = DefaultInsertWriter()
        UpdateWriter = DefaultUpdateWriter()
        DeleteWriter = DefaultDeleteWriter()
        SelectWriter = DefaultSelectWriter()
    }
}
````

* ### Create Entity Class
You should inherit the Entity interface on your
entity class.
````
//The name of table.
@Table("users")
data class User(
    /*
     * here we ignore the id from being included
     * when using the Writer.write...() method.
     * Ignoreon INSERT because we want it to be
     * auto increment and on UPDATE because
     * we do not want to change it.
    /*
    @Ignore(Operation.INSERT, Operation.UPDATE)
    val id: Int,
    val name: String,
    val password: String
) : Entity
````
#### List of Annotations
1. `Table`, Declaring the table's name.
2. `Ignore`, Ignore the annotated property from INSERT or UPDATE or DELETE or SELECT statement or ALL of it (usually when using Writers).
3. `Name`, Declaring the column's name.
4. `FromTheSameTable`, Informing that the property is in the same table. Example :
```
create table persons(
    id int not null primary key auto_increment,
    name varchar(20) not null,
    city varchar(50) null,
    country varchar(50) null
);

--------------------------------

data class Address(
    val city: String,
    val country: String
)

---------------------------------

data class Person(
    val id: Int,
    val name: String,
    @FromTheSameTable
    val address: Address,
)
```
5. `ColumnFrom`, Informing from which table the property is. Example :
```
data class Student(
    val id: Int = 0,
    val name: String,
    val email: String,
    //if the column parameter is empty
    //the property's name will be used, inshaallah (if Allah wills).
    @ColumnFrom(table = "schools", column = "name")
    val schoolName: String
)
```
6. `FromAnotherTable`, Informing that the property is from another table. Example :
```
 * @Table("schools")
 * data class School(
 *  val id: Int = 0,
 *  val name: String,
 *  //...
 * )
 *
 * @Table("students")
 * data class Student(
 *  val id: Int = 0
 *  val name: String,
 *  val email: String,
 *  @FromAnotherTable
 *  val school: School
 * )
 * ```

NOTE: The property that annotated with `ColumnFrom` and `FromAnotherTable` will be ignored by Writers when creating INSERT, UPDATE, and DELETE statement, inshaallah.

* ### Inherit the DMLEditor on the class where you want to write your SQL.
````
class UserDao : DMLEditor {
    private val conn: Connection get() = //...
}
````

* ### Create the Statement.
On insert statement there is an optional ConflictStrategy :

* UPDATE
* REPLACE
* IGNORE

or `null` for no strategy. 

#### Using Writer
````
fun insert(user: User): Int {
    val statement = Writer.writeInsert(user, ConflictStrategy.UPDATE)
    //...
}
````
````
fun update(user: User): Int {
    val statement = Writer.writeInsert(user) { user ->
        where {
            User::id equalTo user.id
        }
    }
    //...
}
````
````
fun delete(user: User): Int {
    val statement = Writer.writeDeletee(user) { user ->
        where {
            User::id equalTo user.id
            and()
            User::name notEqualTo "Administrator"
        }
    }
}
````
````
fun select(userId: Int): List<User> {
    val statement = Writer.writeSelect<User> {
        where {
            User::id greaterEqual userId
        }
        limit(100)
    }
}
````

#### Using Editor
````
fun insert(user: User): Int {
    val statement = insertInto<User> {
        setFields {
            User::name to user.name
            User::password to user.password
        }
    }
    //----
}
````
````
fun update(user: User): Int {
    val statement = update<User> {
        setFields {
            User::name to user.name
            User::password to user.password
        }
        where {
            User::id equalTo user.id
        }
    }
    //...
}
````
````
fun delete(user: User): Int {
    val statement = deleteFrom<User> {
        where {
            User::id equalTo user.id
            and()
            User::name notEqualTo "Administrator"
        }
    }
}
````
````
fun select(userId: Int): List<User> {
    val statement = select {
        take(User::id, User::name, User::password)
        //or take<User>(), but it will ignore the
        //property that annotated with Ignore annotation
        //with SELECT or ALL operation.
        
        from<User>()
        where {
            User::id greaterEqual userId
            and()
            User::name notEqualTo "Administrator"
        }
        limit(100)
    }
}
````

you can also create a statement for INSERT, UPDATE, and DELETE from a list of an entities. **But you later must execute
the PreparedStatement with the executeBatch method.**

* ### Create the PreparedStatement
you can use the prepareStatement extension
````
fun insert(user: User): Int {
    val statement: Statement = //...
    conn.use {
        it.prepareStatement(statement).use { stmt ->
            //...
        }
    }
}
````

* ### The rest is like using a normal PreparedStatement, but Sweet also have some extensions for java.sql.ResultSet
1. Extension to map the ResultSet
````
conn.use {
        it.prepareStatement(statement).use { stmt ->
            val user = stmt.executeQuery().to<User>()
            //...
        }
    }
````
2. Extension to create a List from the ResultSet
````
conn.use {
        it.prepareStatement(statement).use { stmt ->
            val rs = stmt.executeQuery()
            val userList = rs.toList<User>()
            //...
        }
    }
````
3. Extension to create a Sequence from the ResultSet
````
conn.use {
        it.prepareStatement(statement).use { stmt ->
            val rs = stmt.executeQuery()
            val userSequence = rs.toSequence<User>()
            //...
        }
    }
````
3. Extension to create a Flow from the ResultSet
````
conn.use {
        it.prepareStatement(statement).use { stmt ->
            val rs = stmt.executeQuery()
            val userFlow = rs.toFlow<User>()
            //...
        }
    }
````

&copy; Caramel Eat