//                                      Bismillahirrahmanirrahim


import org.jetbrains.kotlin.gradle.tasks.KotlinCompile


plugins {
    kotlin("jvm")
    kotlin("kapt")
    id("com.github.johnrengelman.shadow")
}

val group_name: String by project
val sweet_version: String by project
group = group_name
version = sweet_version

tasks.withType<KotlinCompile>().all {
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

val jarBaseName = "Sweet-Core"
val sourceJar = task("sourceJar", Jar::class) {
    archiveBaseName.set(jarBaseName)
    archiveClassifier.set("sources")

    from(kotlin.sourceSets.main.get().kotlin)
}

tasks.shadowJar {
    archiveBaseName.set(jarBaseName)
    manifest {
        attributes(
            mapOf(
                "Implementation-Title" to "Sweet-Core",
                "Implementation-Version" to archiveVersion.get()
            )
        )
    }

    finalizedBy(sourceJar)
}

dependencies {
    api("org.jetbrains.kotlin:kotlin-stdlib-jdk8:1.6.0")
    api("org.jetbrains.kotlin:kotlin-reflect:1.6.0")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.5.2")

    testImplementation("junit", "junit", "4.13")
}