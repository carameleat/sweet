/*
 * Copyright 2021 Caramel Eat
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

//                                      Bismillahirrahmanirrahim


package com.carameleat.sweet.enums

import com.carameleat.sweet.exceptions.NotSupportedException

enum class ConflictStrategy {

    /**
     * Write query for performing insert if there is no duplicate key, otherwise
     * update with the given value(s).
     *
     * @throws NotSupportedException if the database does not support or if Sweet
	 * doesn't support the database yet for this strategy.
     */
    UPDATE,

    /**
     * Write query for performing insert if there is no duplicate key, otherwise
     * replace with the given value(s).
     *
     * @throws NotSupportedException if the database does not support or if Sweet
	 * doesn't support the database yet for this strategy.
     */
    REPLACE,

    /**
     * Write query for performing insert if there is no duplicate key, otherwise
     * not inserting the given value(s).
     *
     * @throws NotSupportedException if the database does not support or if Sweet
	 * doesn't support the database yet for this strategy.
     */
    IGNORE
}