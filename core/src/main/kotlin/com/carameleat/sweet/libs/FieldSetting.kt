/*
 * Copyright 2021 Caramel Eat
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

//                              Bismillahirrahmanirrahim


package com.carameleat.sweet.libs

interface FieldSetting : Parameterized{

    val sets: List<FieldSet>

    fun isEmpty(): Boolean

    fun isNotEmpty(): Boolean = !isEmpty()
}

interface MutableFieldSetting : FieldSetting, MutableParameterized {

    /**
     * Adding with this method inshaallah will not increase
     * the [Parameter]'s batchSize.
     */
    fun add(set: FieldSet)

    /**
     * Adding with this method inshaallah will increase
     * the [parameter]'s batchSize by 1.
     */
    fun add(set: FieldSet, parameter: Any?)

    /**
     * Adding with this method inshaallah will increase
     * the [Parameter]'s batchSize by the size of [parameters].
     */
    fun add(set: FieldSet, parameters: List<Any?>)

    /**
     * Adding with this method inshaallah will increase
     * the [Parameter]'s batchSize by the bachSize of [setting].
     */
    fun add(setting: FieldSetting)

    /**
     * Clearing this [MutableFieldSetting]
     */
    fun clear()
}

private class FieldSettingImpl(
    override val parameter: Parameter,
    override val sets: List<FieldSet>
) : FieldSetting {
    override fun isEmpty(): Boolean = sets.isEmpty() && parameter.isEmpty()
}

private object EmptyFieldSetting : FieldSetting {
    override val parameter: Parameter
        get() = emptyParameter()
    override val sets: List<FieldSet>
        get() = emptyList()
    override fun isEmpty(): Boolean = true
}

fun FieldSetting(
    parameter: Parameter,
    sets: List<FieldSet>
): FieldSetting = if (parameter.isEmpty() && sets.isEmpty()) EmptyFieldSetting
    else FieldSettingImpl(parameter, sets)

fun emptyFieldSetting(): FieldSetting = EmptyFieldSetting

private class MutableFieldSettingImpl(
    override val parameter: MutableParameter,
    private val _sets: MutableList<FieldSet>,
) : MutableFieldSetting {

    override val sets: List<FieldSet>
        get() = _sets

    override fun isEmpty(): Boolean = sets.isEmpty() && parameter.isEmpty()

    override fun add(set: FieldSet) {
        _sets.add(set)
    }

    override fun add(set: FieldSet, parameter: Any?) {
        add(set)
        this.parameter.add(parameter)
    }

    override fun add(set: FieldSet, parameters: List<Any?>) {
        add(set)
        parameter.add(parameters)
    }

    override fun add(setting: FieldSetting) {
        _sets.addAll(setting.sets)
        parameter.add(setting.parameter)
    }

    override fun clear() {
        _sets.clear()
        parameter.clear()
    }
}

fun MutableFieldSetting.toFieldSetting(): FieldSetting {
    return if (isNotEmpty()) this else EmptyFieldSetting
}

fun MutableFieldSetting(
    parameter: MutableParameter = MutableParameter(),
    sets: MutableList<FieldSet> = mutableListOf(),
): MutableFieldSetting = MutableFieldSettingImpl(parameter, sets)