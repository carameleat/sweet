/*
 * Copyright 2021 Caramel Eat
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

//                              Bismillahirrahmanirrahim


package com.carameleat.sweet.libs

import com.carameleat.sweet.libs.exts.tableColumnName
import com.carameleat.sweet.libs.exts.toColumnSet
import com.carameleat.sweet.libs.entity.Entity
import kotlin.reflect.KProperty1

class FieldSetter {

    private val _setting = MutableFieldSetting()
    val setting: FieldSetting get() = _setting.toFieldSetting()
    var isAddFieldSet = true
        set(value) {
            field = value
            _setting.parameter.isEditBatchSize = value
        }

    infix fun String.to(value: Any?) {
        with(_setting) {
            if (isAddFieldSet) {
                when (value) {
                    is Query -> {
                        add(FieldSet(this@to, value.text), value.parameter.values)
                    }
                    else -> {
                        add(toColumnSet(), value)
                    }
                }
            } else {
                when (value) {
                    is Parameterized -> parameter.add(value.parameter.values)
                    else -> parameter.add(value)
                }
            }
        }
    }

    inline infix fun <reified T : Entity> KProperty1<T, *>.to(value: Any?) {
        tableColumnName to value
    }
}