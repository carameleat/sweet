/*
 * Copyright 2021 Caramel Eat
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

//                              Bismillahirrahmanirrahim


package com.carameleat.sweet.libs.exts

import com.carameleat.sweet.libs.FieldSet
import com.carameleat.sweet.libs.Parameter
import com.carameleat.sweet.libs.Query

fun String.toQuery() = Query(this)

fun String.toQuery(parameter: Parameter) = Query(this, parameter)

fun String.toQuery(parameters: List<Any?>, eachBatchSize: Int = 0) = toQuery(Parameter(parameters, eachBatchSize))

fun String.toQuery(vararg parameters: Any?, eachBatchSize: Int = 0) =
    toQuery(parameters.asList(), eachBatchSize)

fun String.toColumnSet(parameterQuery: String = "?") = FieldSet(this, parameterQuery)

inline fun CharSequence.indexesOf(predicate: (char: Char) -> Boolean): List<Int> {
    val indexes = mutableListOf<Int>().also {
        forEachIndexed { index, char ->
            if (predicate(char)) it.add(index)
        }
    }

    return indexes
}

fun <T> CharSequence.replaceEvery(char: Char, replacements: List<T>): String {
    val charIndexes = indexesOf { it == char }
    val builder = StringBuilder(this)
    var oldInsertIndex = 1 //initial value is 1 to avoid miss insert in the first loop.
    var additionLen = 1 //initial value is 1 to avoid miss insert in the first loop.
    var oldCharIndex = 0
    charIndexes.forEachIndexed { i, charIndex ->
        val insertIndex = oldInsertIndex - 1  + additionLen - 1 +
            charIndex - oldCharIndex + 1
        val addition = replacements[i].toString()
        builder.apply {
            insert(insertIndex, addition)
            deleteAt(insertIndex - 1)
        }

        oldInsertIndex = insertIndex
        additionLen = addition.length
        oldCharIndex = charIndex
    }

    return builder.toString()
}