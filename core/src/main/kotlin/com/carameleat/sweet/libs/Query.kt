/*
 * Copyright 2021 Caramel Eat
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

//                                  Bismillahirrahmanirrahim


package com.carameleat.sweet.libs

import com.carameleat.sweet.libs.exts.replaceEvery
import com.carameleat.sweet.libs.exts.splitTo

/**
 * @param text the query text.
 * @param parameter is the parameters which assigned to the query.
 */
open class Query constructor(
    open val text: String = "",
    override val parameter: Parameter = emptyParameter()
) : Parameterized {

    fun isEmpty(): Boolean = text.isEmpty()

    fun isNotEmpty() = !isEmpty()

    fun batchesToString(isAppendLine: Boolean = true): String {
        if (isNotEmpty()) {
            if (parameter.batchCount == 0) return text
            if (parameter.batchCount == 1) {
                return text.replaceEvery('?', parameter.values)
            }

            val builder = StringBuilder()
            val batchesValues = parameter.values.splitTo(parameter.batchCount)
            batchesValues.forEach {
                val s = text.replaceEvery('?', it)
                builder.apply {
                    if (isAppendLine) {
                        appendLine(s)
                    } else append("| $s |")
                }
            }

            return builder.toString()
        } else return text
    }

    override fun toString(): String {
        return if (isNotEmpty()) {
            if (parameter.batchCount > 1) {
                text
            } else text.replaceEvery('?', parameter.values)
        } else {
            text
        }
    }
}