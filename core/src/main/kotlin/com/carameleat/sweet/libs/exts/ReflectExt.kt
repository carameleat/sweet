/*
 * Copyright 2021 Caramel Eat
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

//                                  Bismillahirrahmanirrahim


package com.carameleat.sweet.libs.exts

import com.carameleat.sweet.annotations.ColumnFrom
import com.carameleat.sweet.annotations.Table
import com.carameleat.sweet.annotations.Name
import com.carameleat.sweet.libs.entity.Entity
import kotlin.reflect.KClass
import kotlin.reflect.KProperty
import kotlin.reflect.KProperty0
import kotlin.reflect.KProperty1
import kotlin.reflect.jvm.isAccessible

val KClass<*>.tableName: String get() {
    return if (annotations.isNotEmpty()) {
        for (ann in annotations) {
            if (ann is Table) return ann.name
        }

        simpleName!!
    } else simpleName!!
}

val KProperty<*>.columnName: String get() {
     return if (annotations.isNotEmpty()) {
        for (ann in annotations) {
            if (ann is Name) return ann.value
            if (ann is ColumnFrom) return ann.column
        }

        name
    } else name
}

inline val <reified T : Entity> KProperty1<T, *>.tableColumnName: String get() {
    return if (annotations.isNotEmpty()) {
        for (ann in annotations) {
            if (ann is Name) return "${T::class.tableName}.${ann.value}"
            if (ann is ColumnFrom) {
                return if (ann.column.isNotEmpty()) {
                    "${ann.table}.${ann.column}"
                } else "${ann.table}.$name"
            }
        }

        "${T::class.tableName}.$name"
    } else "${T::class.tableName}.$name"
}

//Error occurred when using the `isInitialized` PropertyAccessSyntax
@Suppress("UsePropertyAccessSyntax")
internal val KProperty0<*>.isLazyInitialized: Boolean get() {
    val originalAccessible = isAccessible
    isAccessible = true
    val status = (getDelegate() as Lazy<*>).isInitialized()
    isAccessible = originalAccessible

    return status
}