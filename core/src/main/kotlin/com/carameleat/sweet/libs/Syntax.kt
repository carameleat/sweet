/*
 * Copyright 2021 Caramel Eat
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

//                                  Bismillahirrahmanirrahim


package com.carameleat.sweet.libs

import com.carameleat.sweet.libs.exts.tableColumnName
import com.carameleat.sweet.libs.exts.toQuery
import com.carameleat.sweet.libs.entity.Entity
import kotlin.reflect.KProperty1

fun select(statement: Statement): Statement {
    statement.apply {
        wrap()
        writeAt(0, "SELECT ")
    }

    return statement
}

fun sum(column: String): String {
    return "SUM($column)"
}

inline fun <reified T : Entity> sum(column: KProperty1<T, *>) =
    sum(column.tableColumnName)

fun avg(column: String): String {
    return "AVG($column)"
}

inline fun <reified T : Entity> avg(column: KProperty1<T, *>) =
    avg(column.tableColumnName)

fun count(column: String): String {
    return "COUNT($column)"
}

inline fun <reified T : Entity> count(column: KProperty1<T, *>) =
    count(column.tableColumnName)

fun max(column: String): String {
    return "MAX($column)"
}

inline fun <reified T : Entity> max(column: KProperty1<T, *>) =
    max(column.tableColumnName)

fun min(column: String): String {
    return "MIN($column)"
}

inline fun <reified T : Entity> min(column: KProperty1<T, *>) =
    min(column.tableColumnName)

fun exists(statement: Statement): Statement  {
    statement.apply {
        wrap()
        writeAt(0, "EXISTS ")
    }

    return statement
}

fun notExists(statement: Statement): Statement {
    statement.apply {
        wrap()
        writeAt(0, "NOT EXISTS ")
    }

    return statement
}

/*fun ifNull(left: Any?, right: Any): Query {
    return if (left !is Query) {
        if (right !is Query) {
            "IFNULL($left, ?)".toQuery(right)
        } else "IFNULL($left, ${right.text})".toQuery()
    } else {
        if (right !is Query) {
            "IFNULL(?, ?)".toQuery(left, right)
        } else "IFNULL(?, ${right.text})".toQuery(left)
    }
}*/

fun coalesce(vararg values: Any): Query {
    val builder = StringBuilder()
    builder.append("COALESCE(")

    for (i in 0 until values.lastIndex) {
        val value = values[i]

        if (value !is Query) {
            builder.append("?, ")
        } else {
            builder.append("${value.text}, ")
        }
    }

    val value = values.last()
    if (value !is Query) {
        builder.append("?)")
    } else {
        builder.append("${value.text})")
    }

    return builder.toString().toQuery(values.asList())
}

/*infix fun Any.plus(other: Any): Query {
        return mathOperation("+", other)
    }

    infix fun Any.minus(other: Any): Query {
        return mathOperation("-", other)
    }

    infix fun Any.times(other: Any): Query {
        return mathOperation("*", other)
    }

    infix fun Any.div(other: Any): Query {
        return mathOperation("/", other)
    }*/

/*private fun Any.mathOperation(operator: String, other: Any): Query {
    val affected: String
    val effector: String
    val values: MutableList<Any?> = if (this is Query) {
        mutableListOf(this.parameters)
    } else mutableListOf()

    if (this !is Query) {
        affected = "?"
        values.add(this)

        if (other !is Query) {
            effector = "?"
            values.add(other)
        } else effector = other.toString()
    } else {
        affected = toString()

        if (other !is Query) {
            effector = "?"
            values.add(other)
        } else effector = other.toString()
    }

    return "$affected $operator $effector".q(values)
}*/