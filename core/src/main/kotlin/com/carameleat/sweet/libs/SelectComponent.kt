/*
 * Copyright 2021 Caramel Eat
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

//                              Bismillahirrahmanirrahim


package com.carameleat.sweet.libs

interface SelectComponent {

    val fields: List<String>
    val from: String
    val parameter: Parameter
}

interface MutableSelectComponent : SelectComponent {

    fun addField(field: String)

    fun addField(fields: List<String>)

    fun addField(field: String, parameter: Any?)

    fun addField(field: String, parameters: List<Any?>)

    fun addField(field: String, parameter: Parameter)

    fun addField(field: Query)

    fun writeFrom(condition: String)

    fun clear()
}

private class MutableSelectComponentImpl(
    private val _fields: MutableList<String> = mutableListOf(),
    private val joinConditionBuilder: StringBuilder = StringBuilder(),
    private val _parameter: MutableParameter = MutableParameter()
) : MutableSelectComponent {

    override val fields: List<String>
        get() = _fields
    override val from: String
        get() = joinConditionBuilder.toString()
    override val parameter: Parameter
        get() = _parameter

    override fun addField(field: String) {
        _fields.add(field)
    }

    override fun addField(fields: List<String>) {
        _fields.addAll(fields)
    }

    override fun addField(field: String, parameter: Any?) {
        _fields.add(field)
        _parameter.add(parameter)
    }

    override fun addField(field: String, parameters: List<Any?>) {
        _fields.add(field)
        _parameter.add(parameters)
    }

    override fun addField(field: String, parameter: Parameter) {
        _fields.add(field)
        _parameter.add(parameter.values)
    }

    override fun addField(field: Query) {
        _fields.add(field.text)
        _parameter.add(field.parameter)
    }

    override fun writeFrom(condition: String) {
        joinConditionBuilder.appends(" $condition")
    }

    override fun clear() {
        _fields.clear()
        joinConditionBuilder.clear()
        _parameter.clear()
    }
}

fun MutableSelectComponent(
    fields: MutableList<String> = mutableListOf(),
    joinConditionBuilder: StringBuilder = StringBuilder(),
    parameter: MutableParameter = MutableParameter()
): MutableSelectComponent =
    MutableSelectComponentImpl(fields, joinConditionBuilder, parameter)