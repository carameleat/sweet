/*
 * Copyright 2021 Caramel Eat
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

//                                  Bismillahirrahmanirrahim


package com.carameleat.sweet.libs.exts

import com.carameleat.sweet.libs.entity.Entity
import java.io.InputStream
import java.io.Reader
import java.math.BigDecimal
import java.net.URL
import java.sql.*
import java.sql.Array
import java.util.*
import java.util.Date
import kotlin.reflect.KProperty1

inline fun <reified T : Entity> ResultSet.getInt(
    property: KProperty1<T, *>
) = getInt(property.tableColumnName)

inline fun <reified T : Entity> ResultSet.getString(
    property: KProperty1<T, *>
): String? = getString(property.tableColumnName)

inline fun <reified T : Entity> ResultSet.getNString(
    property: KProperty1<T, *>
): String? = getNString(property.tableColumnName)

inline fun <reified T : Entity> ResultSet.getLong(
    property: KProperty1<T, *>
) = getLong(property.tableColumnName)

inline fun <reified T : Entity> ResultSet.getBoolean(
    property: KProperty1<T, *>
) = getBoolean(property.tableColumnName)

inline fun <reified T : Entity> ResultSet.getDouble(
    property: KProperty1<T, *>
) = getDouble(property.tableColumnName)

inline fun <reified T : Entity> ResultSet.getFloat(
    property: KProperty1<T, *>
) = getFloat(property.tableColumnName)

inline fun <reified T : Entity> ResultSet.getDate(
    property: KProperty1<T, *>
): Date? = getDate(property.tableColumnName)

inline fun <reified T : Entity> ResultSet.getDate(
    property: KProperty1<T, *>,
    cal: Calendar
): Date? = getDate(property.tableColumnName, cal)

inline fun <reified T : Entity> ResultSet.getTime(
    property: KProperty1<T, *>
): Time? = getTime(property.tableColumnName)

inline fun <reified T : Entity> ResultSet.getTime(
    property: KProperty1<T, *>,
    cal: Calendar
): Time? = getTime(property.tableColumnName, cal)

inline fun <reified T : Entity> ResultSet.getTimestamp(
    property: KProperty1<T, *>
): Timestamp? = getTimestamp(property.tableColumnName)

inline fun <reified T : Entity> ResultSet.getTimestamp(
    property: KProperty1<T, *>,
    cal: Calendar
): Timestamp? = getTimestamp(property.tableColumnName, cal)

inline fun <reified T : Entity> ResultSet.getBigDecimal(
    property: KProperty1<T, *>
): BigDecimal? = getBigDecimal(property.tableColumnName)

inline fun <reified T : Entity> ResultSet.getArray(
    property: KProperty1<T, *>
): Array? = getArray(property.tableColumnName)

inline fun <reified T : Entity> ResultSet.getAsciiStream(
    property: KProperty1<T, *>
): InputStream? = getAsciiStream(property.tableColumnName)

inline fun <reified T : Entity> ResultSet.getBinaryStream(
    property: KProperty1<T, *>
): InputStream? = getBinaryStream(property.tableColumnName)

inline fun <reified T : Entity> ResultSet.getBlob(
    property: KProperty1<T, *>
): Blob? = getBlob(property.tableColumnName)

inline fun <reified T : Entity> ResultSet.getByte(
    property: KProperty1<T, *>
) = getByte(property.tableColumnName)

inline fun <reified T : Entity> ResultSet.getBytes(
    property: KProperty1<T, *>
): ByteArray? = getBytes(property.tableColumnName)

inline fun <reified T : Entity> ResultSet.getCharacterStream(
    property: KProperty1<T, *>
): Reader? = getCharacterStream(property.tableColumnName)

inline fun <reified T : Entity> ResultSet.getNCharacterStream(
    property: KProperty1<T, *>
): Reader? = getNCharacterStream(property.tableColumnName)

inline fun <reified T : Entity> ResultSet.getClob(
    property: KProperty1<T, *>
): Clob? = getClob(property.tableColumnName)

inline fun <reified T : Entity> ResultSet.getNClob(
    property: KProperty1<T, *>
): NClob? = getNClob(property.tableColumnName)

inline fun <reified T : Entity> ResultSet.getRef(
    property: KProperty1<T, *>
): Ref? = getRef(property.tableColumnName)

inline fun <reified T : Entity> ResultSet.getShort(
    property: KProperty1<T, *>
) = getShort(property.tableColumnName)

inline fun <reified T : Entity> ResultSet.getSQLXML(
    property: KProperty1<T, *>
): SQLXML? = getSQLXML(property.tableColumnName)

inline fun <reified T : Entity> ResultSet.getURL(
    property: KProperty1<T, *>
): URL? = getURL(property.tableColumnName)

inline fun <reified T : Entity> ResultSet.getRowId(
    property: KProperty1<T, *>
): RowId? = getRowId(property.tableColumnName)

inline fun <reified T : Entity> ResultSet.getObject(
    property: KProperty1<T, *>
): Any? = getObject(property.tableColumnName)

inline fun <reified T : Entity, E> ResultSet.getObject(
    property: KProperty1<T, *>,
    type: Class<E>
): Any? = getObject(property.tableColumnName, type)

inline fun <reified T : Entity> ResultSet.getObject(
    property: KProperty1<T, *>,
    map: Map<String, Class<*>>
): Any? = getObject(property.tableColumnName, map)