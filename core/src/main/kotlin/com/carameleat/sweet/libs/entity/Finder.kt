/*
 * Copyright 2021 Caramel Eat
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

//                              Bismillahirrahmanirrahim


@file:Suppress("UNCHECKED_CAST")

package com.carameleat.sweet.libs.entity

import com.carameleat.sweet.annotations.*
import com.carameleat.sweet.enums.Operation
import com.carameleat.sweet.libs.*
import com.carameleat.sweet.libs.exts.contains
import com.carameleat.sweet.libs.exts.tableName
import com.carameleat.sweet.libs.exts.toColumnSet
import kotlin.reflect.KCallable
import kotlin.reflect.KClass
import kotlin.reflect.KProperty
import kotlin.reflect.KProperty1

fun Entity.findFieldSetting(ignoredOperation: Operation): FieldSetting {
    val setting = MutableFieldSetting()

    findSetting(this, ignoredOperation, this::class.members) { name, parameter ->
        setting.add(name.toColumnSet(), parameter)
    }

    return setting
}

//fun <T : Any> findSettingOf(
//    value: T,
//    ignoredOperation: Operation
//): ColumnSetting {
//    val setting = MutableColumnSetting()
//
//    findSetting(value, ignoredOperation, value::class.members) { name, parameter ->
//        setting.add(name.toColumnSet(), parameter)
//    }
//
//    return setting
//}

fun List<Entity>.findFieldSetting(ignoredOperation: Operation): FieldSetting {
    val setting = MutableFieldSetting()
    val firstElement = first()
    val members = firstElement::class.members

    findSetting(firstElement, ignoredOperation, members) { name, value ->
        setting.add(name.toColumnSet(), value)
    }

    setting.parameter.isEditBatchSize = false
    for (i in 1..lastIndex) {
        findSetting(get(i), ignoredOperation, members) { _, value ->
            setting.parameter.add(value)
        }
    }

    return setting
}

//fun <T : Any> findSettingOf(
//    values: List<T>,
//    ignoredOperation: Operation
//): ColumnSetting {
//    val setting = MutableColumnSetting()
//    val members = values.first()::class.members
//
//    findSetting(values.first(), ignoredOperation, members) { name, value ->
//        setting.add(name.toColumnSet(), value)
//    }
//
//    for (i in 1..values.lastIndex) {
//        findSetting(values[i], ignoredOperation, members) { _, value ->
//            setting.add(value)
//        }
//    }
//
//    return setting
//}

inline fun <reified T : Entity> findComponentOf() =
    findComponentOf(T::class)

fun findComponentOf(
    table: KClass<out Entity>
//    isWriteJoinQuery: Boolean = true
): SelectComponent {
    val component = MutableSelectComponent()
    val ignoredOperation = Operation.SELECT
    val tableName = table.tableName
    component.writeFrom(tableName)

//    if (isWriteJoinQuery) {
//        component.writeJoinWithoutSpace(tableName)
//
//        val join = table.findAnnotation<Join>()
//        if (join != null) component.writeJoin(join.query)
//    }

    var field: String?

    outer@ for (member in table.members) {
        field = null

        if (member !is KProperty<*>) { continue }

        if (member.annotations.isNotEmpty()) {
            for (ann in member.annotations) {
                when (ann) {
                    is Ignore -> {
                        if (ignoredOperation in ann) {
                            continue@outer
                        }
                    }
                    is Name -> {
                        field = "$tableName.${ann.value}"
                        break
                    }
                    is ColumnFrom -> {
                        val columnName = if (ann.column.isNotEmpty()) {
                            ann.column
                        } else member.name

                        field = "${ann.table}.$columnName"
                        break
                    }
                    is FromAnotherTable -> {
                        val anotherTable = (member.returnType.classifier as KClass<*>)
                        val anotherTableName = anotherTable.tableName

//                        if (isWriteJoinQuery) {
//                            val anotherJoin = anotherTable.findAnnotation<Join>()
//                            if (anotherJoin != null) component.writeJoin(anotherJoin.query)
//                        }

                        member@ for (anotherMember in anotherTable.members) {
                            if (anotherMember !is KProperty<*>) { continue@member }

                            if (anotherMember.annotations.isNotEmpty()) {
                                for (memberAnn in anotherMember.annotations) {
                                    when (memberAnn) {
                                        is Ignore -> {
                                            if (ignoredOperation in memberAnn) {
                                                continue@member
                                            }
                                        }
                                        is Name -> {
                                            field = "$anotherTableName.${memberAnn.value}"
                                            break
                                        }
                                        is ColumnFrom -> {
                                            val columnName = memberAnn.column.ifEmpty {
                                                member.name
                                            }

                                            field = "${memberAnn.table}.$columnName"
                                            break
                                        }
                                    }
                                }

                                field = field ?: "$anotherTableName.${anotherMember.name}"
                                component.addField(field)
                            } else {
                                component.addField("$anotherTableName.${anotherMember.name}")
                            }

                        }

                        continue@outer
                    }
                    is FromTheSameTable -> {
                        val kClass = (member.returnType.classifier as KClass<*>)

                        member@ for (sameTableMember in kClass.members) {
                            if (sameTableMember !is KProperty<*>) { continue }

                            if (sameTableMember.annotations.isNotEmpty()) {
                                for (memberAnn in sameTableMember.annotations) {
                                    when (memberAnn) {
                                        is Ignore -> {
                                            if (ignoredOperation in memberAnn) {
                                                continue@member
                                            }
                                        }
                                        is Name -> {
                                            field = "$tableName.${memberAnn.value}"
                                            break
                                        }
                                    }
                                }

                                field = field ?: "$tableName.${sameTableMember.name}"
                                component.addField(field)
                            } else {
                                component.addField("$tableName.${sameTableMember.name}")
                            }
                        }

                        continue@outer
                    }
                }
            }

            field = field ?: "$tableName.${member.name}"
            component.addField(field)
        } else {
            component.addField("$tableName.${member.name}")
        }
    }

    return component
}

@PublishedApi
internal inline fun findSetting(
    value: Entity,
    ignoredOperation: Operation,
    members: Collection<KCallable<*>> = value::class.members,
    action: (name: String, parameter: Any?) -> Unit
) {
    var name: String? = null
    var v: Any? = null

    outer@ for (member in members) {
        if (member !is KProperty<*>) { continue }

        if (member.annotations.isNotEmpty()) {
            for (ann in member.annotations) {
                when (ann) {
                    is Ignore -> {
                        if (ignoredOperation in ann) {
                            continue@outer
                        }
                    }
                    is ColumnFrom -> continue@outer
                    is FromAnotherTable -> continue@outer
                    is Name -> {
                        name = ann.value
                        v = (member as KProperty1<Any, *>).get(value)
                        break
                    }
                    is FromTheSameTable -> {
                        val anotherValue = (member as KProperty1<Any, *>).get(value)

                        if (anotherValue != null) {
                            val kClass = anotherValue::class

                            member@ for (anotherMember in kClass.members) {
                                if (anotherMember !is KProperty<*>) {
                                    continue
                                }

                                if (anotherMember.annotations.isNotEmpty()) {
                                    for (memberAnn in anotherMember.annotations) {
                                        when (memberAnn) {
                                            is Ignore -> continue@member
                                            is Name -> {
                                                name = memberAnn.value
                                                v = (anotherMember
                                                    as
                                                    KProperty1<Any, *>).get(anotherValue)
                                            }
                                        }
                                    }

                                    name = name ?: anotherMember.name
                                    action(name, v)
                                } else {
                                    name = anotherMember.name
                                    v = (anotherMember
                                        as
                                        KProperty1<Any, *>).get(anotherValue)

                                    action(name, v)
                                }
                            }
                        }

                        continue@outer
                    }
                }
            }

            name = name ?: member.name
            action(name, v)
        } else {
            name = member.name
            v = (member as KProperty1<Any, *>).get(value)

            action(name, v)
        }
    }
}