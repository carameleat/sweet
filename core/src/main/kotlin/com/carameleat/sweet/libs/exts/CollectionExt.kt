/*
 * Copyright 2021 Caramel Eat
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.carameleat.sweet.libs.exts

fun <T> List<T>.splitTo(count: Int): List<List<T>> {
    if (size % count != 0) {
        val m = "$size divided by $count"
        throw IllegalArgumentException(m)
    }

    val subListSize = size / count
    val list = mutableListOf<MutableList<T>>().also {
        var index = 0
        for (i in 0 until count) {
            val subList = mutableListOf<T>()
            for (u in 0 until subListSize) {
                val element = get(index)
                subList.add(element)
                index++
            }

            it.add(subList)
        }
    }

    return list
}