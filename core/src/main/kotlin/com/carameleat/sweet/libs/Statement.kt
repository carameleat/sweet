/*
 * Copyright 2021 Caramel Eat
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

//                                  Bismillahirrahmanirrahim


package com.carameleat.sweet.libs

/**
 * A class that hold the materials or what is needed to make a [java.sql.PreparedStatement].
 */
class Statement : Query(), MutableParameterized, QueryTextMaker {

    override val parameter: MutableParameter = MutableParameter()

    /**
     * A [StringBuilder] that contains the SQL query.
     */
    @PublishedApi
    internal val queryBuilder: StringBuilder = StringBuilder()

    override val text: String get() = queryBuilder.toString()

    override var isWriteQueryText = true
        set(value) {
            field = value
            parameter.isEditBatchSize = value
        }

    /**
     * Append the [text] to the [queryBuilder] with extra whitespace
     * before it.
     */
    fun write(text: String) {
        if (isWriteQueryText) {
            queryBuilder.appends(text)
        }
    }

    fun writeAt(offset: Int, text: String) {
        if (isWriteQueryText) queryBuilder.inserts(offset, text)
    }

    fun write(text: String, parameter: Any?) {
        write(text)
        this.parameter.add(parameter)
    }

    fun writeAt(textOffset: Int, text: String, paramIndex: Int, parameter: Any?) {
        writeAt(textOffset, text)
        this.parameter.add(paramIndex, parameter)
    }

    fun write(text: String, parameters: List<Any?>) {
        write(text)
        this.parameter.add(parameters)
    }

    fun writeAt(textOffset: Int, text: String, parameterIndex: Int, parameters: List<Any?>) {
        writeAt(textOffset, text)
        this.parameter.add(parameterIndex, parameters)
    }

    fun write(text: String, parameter: Parameter) {
        write(text)
        this.parameter.add(parameter)
    }

    fun writeAt(textOffset: Int, text: String, startIndex: Int, parameter: Parameter) {
        writeAt(textOffset, text)
        this.parameter.add(startIndex, parameter)
    }

    fun write(statement: Statement) {
        statement.let {
            write("( ${it.text} )", it)
        }
    }

    /**
     * Perform the [writeAt] method which has
     * ```
     * text: String, parameters: List<Any?>, eachValueParameterSize: Int
     * ```
     * parameters.
     */
    fun write(query: Query) = with(query) { write(text, parameter) }

    fun wrap() {
        writeAt(0, "( ")
        write(")")
    }

    fun clear() {
        queryBuilder.clear()
        parameter.clear()
    }
}