/*
 * Copyright 2021 Caramel Eat
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.carameleat.sweet.libs

fun StringBuilder.appends(text: String): StringBuilder {
    if (isEmpty()) {
        append(text)
    } else {
        val mText = if (last() == ' ') text else " $text"
        append(mText)
    }

    return this
}

fun StringBuilder.inserts(offset: Int, text: String): StringBuilder {
    if (isEmpty()) {
        insert(offset, text)
    } else {
        val mText = if (last() == ' ') {
            text
        } else if (offset == 0) {
            text
        } else " $text"

        insert(offset, mText)
    }

    return this
}