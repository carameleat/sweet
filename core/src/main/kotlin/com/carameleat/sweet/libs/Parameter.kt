/*
 * Copyright 2021 Caramel Eat
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

//                                  Bismillahirrahmanirrahim


package com.carameleat.sweet.libs

interface Parameter {

    val values: List<Any?>
    val batchSize: Int
    val batchCount: Int
        get() = if (batchSize > 0) values.size / batchSize
        else 0

    /**
     * [IntRange] of 1..[batchSize].
     */
    val valuesIndices: IntRange get() = 1..batchSize

    /**
     * [IntRange] of 1..[batchCount].
     */
    val batchIndices get() = 1..batchCount

    /**
     * @param valueIndex the index of the parameter value. *Start from 1.
     */
    operator fun get(valueIndex: Int): Any? = values[valueIndex]

    /**
     * @param batchIndex the index of the value. *Start from 1.
     * @param valueIndex the index of the parameter value. *Start from 1.
     */
    operator fun get(batchIndex: Int, valueIndex: Int): Any? {
        val index = (valueIndex + (batchSize * (batchIndex - 1))) - 1

        return values[index]
    }

    fun isEmpty(): Boolean = values.isEmpty()

    fun isNotEmpty() = !isEmpty()
}

private class ParameterImpl(
    override val values: List<Any?>,
    override val batchSize: Int,
    override val batchCount: Int
) : Parameter

internal object EmptyParameter : Parameter {
    override val values: List<Any?>
        get() = emptyList()
    override val batchSize: Int
        get() = 0
    override val batchCount: Int
        get() = 0
}

fun Parameter(
    value: List<Any?> = emptyList(),
    batchSize: Int = 0,
    batchCount: Int = 0
): Parameter {
    return if (value.isNotEmpty() && batchSize > 0 && batchCount == 0) {
        ParameterImpl(value, batchSize, batchCount)
    } else emptyParameter()
}

fun emptyParameter(): Parameter = EmptyParameter

interface MutableParameter : Parameter {

    var isEditBatchSize: Boolean

    /**
     * @param valueIndex the index of the parameter value. *Start from 1.
     * @param value the new parameter value.
     */
    operator fun set(valueIndex: Int, value: Any?)

    /**
     * @param batchIndex the index of the value. *Start from 1.
     * @param valueIndex the index of the parameter value. *Start from 1.
     * @param value the new parameter value.
     */
    operator fun set(batchIndex: Int, valueIndex: Int, value: Any?)

    fun add(value: Any?)

    fun add(index: Int, value: Any?)

    fun add(values: List<Any?>)

    fun add(index: Int, values: List<Any?>)

    fun add(parameter: Parameter)

    fun add(startIndex: Int, parameter: Parameter)

    fun remove(value: Any?)

    /**
     * Remove the parameter at the given [index].
     * NOTE: This method is not decreasing the [batchSize].
     */
    fun removeAt(index: Int)

    fun clear()
}

private class MutableParameterImpl(
    private val _values: MutableList<Any?>,
    override var batchSize: Int = 0
): MutableParameter {

    override val values: List<Any?>
        get() = _values
    override var isEditBatchSize: Boolean = true

    override fun set(valueIndex: Int, value: Any?) {
        checkParameterIndex(valueIndex)
        _values[valueIndex - 1] = value
    }

    /**
     * @param batchIndex the index of the value. *Start from 1.
     * @param valueIndex the index of the parameter value. *Start from 1.
     * @param value the new parameter value.
     */
    override fun set(batchIndex: Int, valueIndex: Int, value: Any?) {
        checkBatchIndex(batchIndex)
        checkParameterIndex(valueIndex)

        val index = (valueIndex + (batchSize * (batchIndex - 1))) - 1
        _values[index] = value
    }

    override fun add(value: Any?) {
        if (isEditBatchSize) {
            batchSize++
        }

        _values.add(value)
    }

    override fun add(index: Int, value: Any?) {
        checkParameterIndex(index)
        if (isEditBatchSize) {
            batchSize++
        }

        _values.add(index - 1, value)
    }

    override fun add(values: List<Any?>) {
        if (values.isNotEmpty()) {
            if (isEditBatchSize) {
                batchSize += values.size
            }

            _values.addAll(values)
        }
    }

    override fun add(index: Int, values: List<Any?>) {
        checkParameterIndex(index)

        if (values.isNotEmpty()) {
            if (isEditBatchSize) {
                batchSize += values.size
            }

            _values.addAll(index - 1, values)
        }
    }

    override fun add(parameter: Parameter) {
        if (parameter.isNotEmpty()) {
            val values = parameter.values

            if (batchCount > 1) {
                val batchCount = batchCount

                if (parameter.batchCount != batchCount) {
                    println("${parameter.batchCount} : $batchCount")

                    val message = "Parameter's batchCount is not equal to " +
                        "Statement's batchCount."
                    throw IllegalArgumentException(message)
                }

                if (_values.isNotEmpty()) {
                    var insertIndex = batchSize + 1
                    var newParameterIndex = 0

                    for (i in 0 until batchCount) {
                        for (u in 0 until parameter.batchSize) {
                            if (i < batchCount - 1) {
                                val mIndex = insertIndex - 1
                                checkParameterIndex(mIndex)

                                _values.add(mIndex, values[newParameterIndex])
                            } else {
                                _values.add(values[newParameterIndex])
                            }

                            newParameterIndex++
                            insertIndex++
                        }

                        insertIndex += batchSize
                    }
                } else _values.addAll(values)
            } else _values.addAll(values)

            if (isEditBatchSize) {
                batchSize += parameter.batchSize
            }
        }
    }

    override fun add(startIndex: Int, parameter: Parameter) {
        if (parameter.isNotEmpty()) {
            val values = parameter.values

            if (batchCount > 1) {
                val batchCount = batchCount

                if (parameter.batchCount != batchCount) {
                    val message = "Parameter's batchCount is not equal to " +
                        "Statement's batchCount."
                    throw IllegalArgumentException(message)
                }

                if (_values.isNotEmpty()) {
                    var insertIndex = startIndex
                    var newParameterIndex = 0

                    for (i in 0 until batchCount) {
                        for (u in 0 until parameter.batchSize) {
                            if (i < batchCount - 1) {
                                val mIndex = insertIndex - 1
                                checkParameterIndex(mIndex)

                                _values.add(mIndex, values[newParameterIndex])
                            } else {
                                _values.add(values[newParameterIndex])
                            }

                            newParameterIndex++
                            insertIndex++
                        }

                        insertIndex += batchSize
                    }
                }
            } else {
                val mIndex = startIndex - 1
                checkParameterIndex(mIndex)

                _values.add(mIndex, values)
            }

            if (isEditBatchSize) {
                batchSize += parameter.batchSize
            }
        }
    }

    override fun remove(value: Any?) {
        if (isEditBatchSize) batchSize--

        _values.remove(value)
    }

    /**
     * Remove the parameter at the given [index].
     * NOTE: This method is not decreasing the [batchSize].
     */
    override fun removeAt(index: Int) {
        checkParameterIndex(index)
        if (isEditBatchSize) batchSize--

        _values.removeAt(index - 1)
    }

    override fun clear() {
        if (_values.isNotEmpty()) {
            _values.clear()
            batchSize = 0
        }
    }

    private fun checkBatchIndex(index: Int) {
        if (index < 1 || index > batchCount) {
            throw IndexOutOfBoundsException(
                "index (start from 1): $index, " +
                    "batch's size: ${values.size}"
            )
        }
    }

    private fun checkParameterIndex(index: Int) {
        if (index < 1 || index > values.size) {
            throw IndexOutOfBoundsException(
                "index (start from 1): $index, " +
                    "value parameter's size: ${values.size}"
            )
        }
    }
}

fun MutableParameter(
    values: MutableList<Any?> = mutableListOf(),
    batchSize: Int = 0
): MutableParameter {
    return MutableParameterImpl(values, batchSize)
}