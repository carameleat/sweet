/*
 * Copyright 2021 Caramel Eat
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

//                                  Bismillahirrahmanirrahim


package com.carameleat.sweet.libs.exts

import com.carameleat.sweet.annotations.Ignore
import com.carameleat.sweet.enums.Operation

/**
 * @return [true] if the [Ignore] annotation contains the given [operation] or ALL [Operation].
 */
operator fun Ignore.contains(operation: Operation): Boolean {
    for (type in operations) {
        if (type == Operation.ALL || type == operation) {
            return true
        }
    }

    return false
}