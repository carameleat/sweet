/*
 * Copyright 2021 Caramel Eat
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

//                                  Bismillahirrahmanirrahim


package com.carameleat.sweet.writers

import com.carameleat.sweet.libs.FieldSetting
import com.carameleat.sweet.libs.Query
import com.carameleat.sweet.libs.Statement
import com.carameleat.sweet.libs.entity.Entity

interface UpdateWriter {

    fun <T : Entity> write(value: T, condition: Query): Statement

    fun <T : Entity> write(values: List<T>, condition: Query): Statement

    fun writeTo(
        statement: Statement,
        table: String,
        setting: FieldSetting,
        condition: Query
    ): Statement
}