/*
 * Copyright 2021 Caramel Eat
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.carameleat.sweet.writers

import com.carameleat.sweet.libs.FieldSetter
import com.carameleat.sweet.editors.HighEditor
import com.carameleat.sweet.editors.dml.SelectEditor
import com.carameleat.sweet.enums.ConflictStrategy
import com.carameleat.sweet.libs.Statement
import com.carameleat.sweet.libs.entity.Entity

object Writer {

    lateinit var InsertWriter: InsertWriter
    lateinit var UpdateWriter: UpdateWriter
    lateinit var DeleteWriter: DeleteWriter
    lateinit var SelectWriter: SelectWriter

    fun writeInsert(
        entity: Entity,
        conflictStrategy: ConflictStrategy? = null
    ): Statement {
        return InsertWriter.write(entity, conflictStrategy)
    }

    fun <T : Entity> writeInsert(
        entity: T,
        updateStrategy: FieldSetter.(model: T) -> Unit
    ): Statement {
        val setter = FieldSetter().also {
            it.updateStrategy(entity)
        }

        return InsertWriter.write(entity, setter.setting)
    }

    fun writeInsert(
        entities: List<Entity>,
        conflictStrategy: ConflictStrategy? = null
    ): Statement {
        return InsertWriter.write(entities, conflictStrategy)
    }

    fun <T : Entity> writeInsert(
        entities: List<T>,
        updateStrategy: FieldSetter.(value: T) -> Unit
    ): Statement {
        val setter = FieldSetter().apply {
            updateStrategy(entities.first())
            isAddFieldSet = false

            for (i in 1..entities.lastIndex) {
                updateStrategy(entities[i])
            }
        }

        return InsertWriter.write(entities, setter.setting)
    }

    fun <T : Entity> writeUpdate(
        entity: T,
        condition: HighEditor.(entity: T) -> Unit = {}
    ): Statement {
        val setter = HighEditor().also {
            it.condition(entity)
        }

        return UpdateWriter.write(entity, setter.query)
    }

    fun <T : Entity> writeUpdate(
        entities: List<T>,
        condition: HighEditor.(value: T) -> Unit
    ): Statement {
        val setter = HighEditor().apply {
            condition(entities.first())
            isWriteQueryText = false

            for (i in 1..entities.lastIndex) {
                condition(entities[i])
            }
        }

        return UpdateWriter.write(entities, setter.query)
    }

    fun <T : Entity> writeDelete(entity: T, condition: HighEditor.(element: T) -> Unit): Statement {
        val setter = HighEditor().also { it.condition(entity) }

        return DeleteWriter.write(entity, setter.query)
    }

    fun <T : Entity> writeDelete(entities: List<T>, condition: HighEditor.(element: T) -> Unit): Statement {
        val setter = HighEditor().apply {
            condition(entities.first())
            isWriteQueryText = false

            for (i in 1..entities.lastIndex) {
                condition(entities.get(i))
            }
        }

        return DeleteWriter.write(entities, setter.query)
    }

    inline fun <reified T : Entity> select(
        condition: SelectEditor.Setter.() -> Unit = {}
    ): Statement {
        val setter = SelectEditor.Setter().also {
            it.condition()
        }

        return SelectWriter.write(T::class, setter.query)
    }
}