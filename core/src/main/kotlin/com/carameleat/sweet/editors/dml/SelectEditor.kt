/*
 * Copyright 2021 Caramel Eat
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

//                              Bismillahirrahmanirrahim


package com.carameleat.sweet.editors.dml

import com.carameleat.sweet.editors.ConditionEditor
import com.carameleat.sweet.editors.HighEditor
import com.carameleat.sweet.editors.JoinEditor
import com.carameleat.sweet.libs.MutableSelectComponent
import com.carameleat.sweet.libs.Query
import com.carameleat.sweet.libs.SelectComponent
import com.carameleat.sweet.libs.Statement
import com.carameleat.sweet.libs.exts.tableColumnName
import com.carameleat.sweet.libs.exts.tableName
import com.carameleat.sweet.libs.entity.Entity
import com.carameleat.sweet.libs.entity.findComponentOf
import kotlin.reflect.KProperty1

interface SelectEditor {

    val selectSetter: Setter get() = Setter()

    fun select(component: SelectComponent, condition: Query): Statement

    open class Setter : HighEditor() {

        private val mComponent: MutableSelectComponent = MutableSelectComponent()
        @Suppress("PropertyName")
        @PublishedApi
        internal val _component get() = mComponent
        val component: SelectComponent get() = _component
        protected open val joinEditor: JoinEditor get() = JoinEditor()
        @Suppress("PropertyName")
        @PublishedApi
        internal val _joinEditor get() = joinEditor

        fun take(field: String) {
            _component.addField(field)
        }

        fun take(vararg fields: String) {
            fields.forEach { take(it) }
        }

        inline fun <reified T : Entity> take(field: KProperty1<T, *>) {
            take(field.tableColumnName)
        }

        inline fun <reified T : Entity> take(vararg fields: KProperty1<T, *>) {
            fields.forEach { take(it) }
        }

        fun take(field: Query) {
            _component.addField(field)
        }

        fun take(vararg fields: Query) {
            fields.forEach { take(it) }
        }

        inline fun <reified T : Entity> take() {
            val fields = findComponentOf<T>().fields
            _component.addField(fields)
        }

        inline fun <reified F: Entity, reified S : Entity> take2() {
            take<F>()
            take<S>()
        }

        inline fun <reified F : Entity, reified S : Entity, reified T : Entity> take3() {
            take<F>()
            take<S>()
            take<T>()
        }

        fun take(field: Statement) {
            field.wrap()
            _component.addField(field)
        }

        inline fun from(table: String, joinSetting: JoinEditor.() -> Unit = {}) {
            val joinQuery = _joinEditor.apply(joinSetting).query.text
            val text = if (joinQuery.isNotEmpty()) {
                table + joinQuery
            } else table

            _component.writeFrom(text)
        }

        inline fun <reified T : Entity> from(joinSetting: JoinEditor.() -> Unit = {}) {
            from(T::class.tableName, joinSetting)
        }

        open fun having(condition: String) {
            write("HAVING $condition")
        }

        inline fun <reified T : Entity> having(column: KProperty1<T, *>) =
            having(column.tableColumnName)

        fun having(query: Query) {
            with(query) {
                having(text)
                add(parameter.values)
            }
        }

        inline fun having(condition: ConditionEditor.() -> Unit) {
            val query = ConditionEditor().apply(condition).query
            having(query)
        }

        open fun groupBy(fields: List<String>) {
            write("GROUP BY")
            if (fields.isNotEmpty()) {
                fields.forEachIndexed { i, field ->
                    val text = if (i != fields.lastIndex) {
                        "$field,"
                    } else field

                    write(text)
                }
            }
        }

        inline fun groupBy(setting: GroupingEditor.() -> Unit) {
            val fields = GroupingEditor().apply(setting).fields
            groupBy(fields)
        }

        open fun orderBy(fields: List<String>) {
            write("ORDER BY")
            if (fields.isNotEmpty()) {
                fields.forEachIndexed { i, field ->
                    val text = if (i != fields.lastIndex) {
                        "$field,"
                    } else field

                    write(text)
                }
            }
        }

        inline fun orderBy(setting: OrderEditor.() -> Unit) {
            val fields = OrderEditor().apply(setting).fields
            orderBy(fields)
        }

        open fun offset(value: Int) {
            write("OFFSET ?", value)
        }

        class GroupingEditor {

            private val _fields = mutableListOf<String>()
            val fields: List<String> get() = _fields

            fun by(field: String) {
                _fields.add(field)
            }

            fun by(fields: List<String>) {
                _fields.addAll(fields)
            }

            fun by(vararg fields: String) {
                fields.forEach { by(it) }
            }

            inline fun <reified T : Entity> by(field: KProperty1<T, *>) {
                by(field.tableColumnName)
            }

            inline fun <reified T : Entity> by(vararg fields: KProperty1<T, *>) {
                fields.forEach { by(it) }
            }
        }

        class OrderEditor {

            private val _fields = mutableListOf<String>()
            val fields: List<String> get() = _fields

            fun String.asc() {
                _fields.add("$this ASC")
            }

            fun String.desc() {
                _fields.add("$this DESC")
            }

            inline fun <reified T : Entity> KProperty1<T, *>.asc() {
                tableColumnName.asc()
            }

            inline fun <reified T : Entity> KProperty1<T, *>.desc() {
                tableColumnName.desc()
            }
        }
    }
}

inline fun SelectEditor.select(setting: SelectEditor.Setter.() -> Unit): Statement {
    val setter = selectSetter.apply(setting)

    return select(setter.component, setter.query)
}