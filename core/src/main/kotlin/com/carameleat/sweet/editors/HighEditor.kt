/*
 * Copyright 2021 Caramel Eat
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

//                                  Bismillahirrahmanirrahim


package com.carameleat.sweet.editors

import com.carameleat.sweet.libs.Query
import com.carameleat.sweet.libs.Statement
import com.carameleat.sweet.libs.exts.toQuery

open class HighEditor : LowEditor() {

    override val query: Query
        get() = queryBuilder.toString().toQuery(parameter)
    override var isWriteQueryText get() = super.isWriteQueryText
        set(value) {
            super.isWriteQueryText = value
            conditionEditor.isWriteQueryText = value
        }
    protected open val conditionEditor = ConditionEditor()
    @PublishedApi
    @Suppress("PropertyName")
    internal val _conditionEditor get() = conditionEditor

    inline fun where(condition: (ConditionEditor.() -> Unit)) {
        if (isWriteQueryText) {
            write("WHERE")

            val query = _conditionEditor.apply(condition).query
            with(query) {
                write(text, parameter.values)
            }

            _conditionEditor.clearParameters()
        } else {
            val parameter = _conditionEditor.apply(condition).parameter
            add(parameter.values)

            _conditionEditor.clearParameters()
        }
    }

    open fun limit(count: Int) {
        if (isWriteQueryText) write("LIMIT ?", count)
        else add(count)
    }

    open infix fun Any?.alias(name: String): Query {
        return when (this) {
            !is Query -> {
                "? AS $name".toQuery(this)
            }
            is Statement -> {
                wrap()
                write("AS $name")

                this
            }
            else -> "$this AS $name".toQuery(this)
        }
    }
}