/*
 * Copyright 2021 Caramel Eat
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

//                                      Bismillahirrahmanirrahim


package com.carameleat.sweet.editors

import com.carameleat.sweet.libs.*
import com.carameleat.sweet.libs.entity.Entity
import com.carameleat.sweet.libs.exts.tableColumnName
import kotlin.reflect.KProperty
import kotlin.reflect.KProperty1

open class ConditionEditor : LowEditor() {

    open fun and() {
        if (isWriteQueryText) write("AND")
    }

    open fun or() {
        if (isWriteQueryText) write("OR")
    }

    open fun xor() {
        if (isWriteQueryText) write("XOR")
    }

    inline fun not(condition: () -> Unit) {
        if (isWriteQueryText) write("NOT")
        wrap(condition)
    }

    open infix fun String.equalTo(value: Any?) {
        if (isWriteQueryText) writeQuery(this, "=", value)
        else addParameter(value)
    }

    inline infix fun <reified T : Entity> KProperty1<T, *>.equalTo(value: Any?) {
        if (isWriteQueryText) writeQuery(tableColumnName, "=", value)
        else addParameter(value)
    }

    infix fun String.notEqualTo(value: Any?) {
        if (isWriteQueryText) writeQuery(this, "<>", value)
        else addParameter(value)
    }

    inline infix fun <reified T : Entity> KProperty1<T, *>.notEqualTo(value: Any?) {
        if (isWriteQueryText) writeQuery(tableColumnName, "<>", value)
        else addParameter(value)
    }

    open infix fun String.greaterThan(value: Any?) {
        if (isWriteQueryText) writeQuery(this, ">", value)
        else addParameter(value)
    }

    inline infix fun <reified T : Entity> KProperty1<T, *>.greaterThan(value: Any?) {
        if (isWriteQueryText) writeQuery(tableColumnName, ">", value)
        else addParameter(value)
    }

    open infix fun String.greaterEqual(value: Any?) {
        if (isWriteQueryText) writeQuery(this, ">=", value)
        else addParameter(value)
    }

    inline infix fun <reified T : Entity> KProperty1<T, *>.greaterEqual(value: Any?) {
        if (isWriteQueryText) writeQuery(tableColumnName, ">=", value)
        else addParameter(value)
    }

    open infix fun String.lessThan(value: Any?) {
        if (isWriteQueryText) writeQuery(this, "<", value)
        else addParameter(value)
    }

    inline infix fun <reified T : Entity> KProperty1<T, *>.lessThan(value: Any?) {
        if (isWriteQueryText) writeQuery(tableColumnName,"<", value)
        else addParameter(value)
    }

    open infix fun String.lessEqual(value: Any?) {
        if (isWriteQueryText) writeQuery(this, "<=", value)
        else addParameter(value)
    }

    inline infix fun <reified T : Entity> KProperty1<T, *>.lessEqual(value: Any?) {
        if (isWriteQueryText) writeQuery(tableColumnName, "<=", value)
        else addParameter(value)
    }

    open infix fun String.isIn(statement: Statement) {
        if (isWriteQueryText) writeQuery(this, "IN", statement)
        else addParameter(statement.parameter.values)
    }

    inline infix fun <reified T : Entity> KProperty1<T, *>.isIn(statement: Statement) {
        if (isWriteQueryText) writeQuery(tableColumnName, "IN", statement)
        else addParameter(statement.parameter.values)
    }

    open infix fun String.isIn(values: List<Any?>) {
        if (isWriteQueryText) writeQuery(this, "IN", values)
        else addParameter(values)
    }

    fun String.isIn(vararg values: Any?) {
        isIn(values.asList())
    }

    inline infix fun <reified T : Entity> KProperty1<T, *>.isIn(values: List<Any?>) {
        if (isWriteQueryText) writeQuery(tableColumnName, "IN", values)
        else addParameter(values)
    }

    inline fun <reified T : Entity> KProperty1<T, *>.isIn(vararg values: Any?) {
        isIn(values.asList())
    }

    open infix fun String.like(pattern: String) {
        if (isWriteQueryText) writeQuery(this, "LIKE", pattern)
        else addParameter(pattern)
    }

    inline infix fun <reified T : Entity> KProperty1<T, *>.like(pattern: String) {
        if (isWriteQueryText) writeQuery(tableColumnName, "LIKE", pattern)
        else addParameter(pattern)
    }

    inline infix fun String.between(condition: () -> Unit) {
        if (isWriteQueryText) write("$this BETWEEN")
        condition()
    }

    inline infix fun <reified T : Entity> KProperty1<T, *>.between(condition: () -> Unit) {
        if (isWriteQueryText) write("$tableColumnName BETWEEN")
        condition()
    }

    /**
     * Method for writing the "{value1} and {value2}" condition in
     * [between] method.
     */
    @Suppress("UNCHECKED_CAST")
    open infix fun Any?.n(value: Any?) {
        when (this) {
            is KProperty1<*, *> -> {
                if (isWriteQueryText) {
                    val compared = (this as KProperty1<Entity, *>).tableColumnName
                    write(compared)
                }
            }
            is Query -> {
                if (isWriteQueryText) write(text, parameter.values)
                else add(parameter.values)
            }
            is Statement -> {
                if (this@ConditionEditor.isWriteQueryText) {
                    wrap()
                    this@ConditionEditor.write(text, parameter.values)
                } else this@ConditionEditor.add(parameter.values)
            }
            else -> {
                if (isWriteQueryText) write("?", this)
                else add(this)
            }
        }

        if (isWriteQueryText) write("AND")

        when (value) {
            is KProperty1<*, *> -> {
                if (isWriteQueryText) {
                    val compared = (value as KProperty1<Entity, *>).tableColumnName
                    write(compared)
                }
            }
            is Statement -> {
                if (isWriteQueryText) {
                    value.wrap()
                    write(value.text, value.parameter.values)
                } else add(value.parameter.values)
            }
            is Query -> {
                if (isWriteQueryText) write(value.text, value.parameter.values)
                else add(value.parameter.values)
            }
            else -> {
                if (isWriteQueryText) write("?", value)
                else add(value)
            }
        }
    }

    /**
     * Write the condition query.
     *
     * @param compared the compared value/field.
     * @param operator the comparison or condition operator.
     * @param value the value or the comparison value/field.
     */
    @Suppress("UNCHECKED_CAST")
    open fun writeQuery(compared: String, operator: String, value: Any?) {
        when (value) {
            is KProperty1<*, *> -> {
                val comparison = (value as KProperty1<Entity, *>).tableColumnName
                write("$compared $operator $comparison")
            }
            is Statement -> {
                value.let {
                    it.wrap()
                    write("$compared $operator ${it.text}", it.parameter.values)
                }
            }
            is Query -> {
                write("$compared $operator $value", value.parameter.values)
            }
            else -> {
                write("$compared $operator ?", value)
            }
        }
    }

    /**
     * Write the condition query.
     *
     * @param compared the compared value/field.
     * @param operator the comparison or condition operator.
     * @param values list of comparison value/field.
     */
    @Suppress("UNCHECKED_CAST")
    open  fun writeQuery(compared: String, operator: String, values: List<Any?>) {
        write("$this $operator (")

        if (values.isNotEmpty()) {
            values.forEachIndexed { i, it ->
                when (it) {
                    is KProperty1<*, *> -> {
                        val value = (it as KProperty1<Entity, *>).tableColumnName
                        val text = createText(i, values.lastIndex, value)
                        write(text)
                    }
                    is Statement -> {
                        it.wrap()

                        val text = createText(i, values.lastIndex, it.text)
                        write(text, it.parameter.values)
                    }
                    is Query -> {
                        val text = createText(i, values.lastIndex, it.text)
                        write(text, it.parameter.values)
                    }
                    else -> {
                        val text = createText(i, values.lastIndex, "?")
                        write(text, it)
                    }
                }
            }
        } else write(")")
    }

    @PublishedApi
    internal fun addParameter(value: Any?) {
        if (value !is Parameterized) {
            if (value !is KProperty<*>) add(value)
        } else {
            add(value.parameter.values)
        }
    }

    private fun createText(
        currentIndex: Int,
        lastIndex: Int,
        text: String
    ): String = if (currentIndex != lastIndex) "$text," else "$text )"

    @PublishedApi
    internal fun addParameter(values: List<Any?>) {
        if (values.isNotEmpty()) {
            values.forEach {
                addParameter(it)
            }
        }
    }

    override fun write(text: String, parameter: Any?) {
        write(text)
        add(parameter)
    }

    override fun write(text: String, parameters: List<Any?>) {
        write(text)
        add(parameters)
    }

    override fun write(text: String) {
        queryBuilder.appends(text)
    }

    override fun add(parameter: Any?) {
        this.parameter.add(parameter)
    }

    override fun add(parameters: List<Any?>) {
        if (parameters.isNotEmpty()) {
            parameter.add(parameters)
        }
    }

    fun clearParameters() { if (parameter.isNotEmpty()) parameter.clear() }
}