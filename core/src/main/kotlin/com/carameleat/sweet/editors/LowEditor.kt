/*
 * Copyright 2021 Caramel Eat
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

//                                  Bismillahirrahmanirrahim


package com.carameleat.sweet.editors

import com.carameleat.sweet.libs.*
import com.carameleat.sweet.libs.exts.toQuery

abstract class LowEditor : QueryTextMaker, MutableParameterized {

    protected val queryBuilder = StringBuilder()
    override val parameter = MutableParameter()
    override var isWriteQueryText: Boolean = true
        set(value) {
            field = value
            parameter.isEditBatchSize = value
        }
    open val query: Query get() = queryBuilder.toString().toQuery(parameter)

    inline fun wrap(block: () -> Unit) {
        if (isWriteQueryText) {
            write("( ")
            block()
            write(")")
        } else block()
    }

    open fun add(parameter: Any?) {
        this.parameter.add(parameter)
    }

    open fun add(parameters: List<Any?>) {
        if (parameters.isNotEmpty()) {
            this.parameter.add(parameters)
        }
    }

    open fun write(text: String, parameter: Any?) {
        write(text)
        add(parameter)
    }

    open fun write(text: String, parameters: List<Any?>) {
        write(text)
        add(parameters)
    }

    open fun write(text: String) {
        queryBuilder.appends(text)
    }

    fun clear() {
        if (queryBuilder.isNotEmpty()) queryBuilder.clear()
        if (parameter.isNotEmpty()) parameter.clear()
    }
}