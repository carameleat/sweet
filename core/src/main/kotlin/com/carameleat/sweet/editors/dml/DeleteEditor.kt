/*
 * Copyright 2021 Caramel Eat
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

//                              Bismillahirrahmanirrahim


package com.carameleat.sweet.editors.dml

import com.carameleat.sweet.editors.HighEditor
import com.carameleat.sweet.editors.JoinEditor
import com.carameleat.sweet.libs.Query
import com.carameleat.sweet.libs.Statement
import com.carameleat.sweet.libs.exts.tableName
import com.carameleat.sweet.libs.entity.Entity

interface DeleteEditor {

    val deleteSetter: Setter get() = Setter()

    fun deleteFrom(table: String, condition: Query, joinCondition: String? = null): Statement

    open class Setter : HighEditor() {

        var joinCondition: String? = null
            private set

        fun joinTable(condition: String) {
            joinCondition = condition
        }

        inline fun joinTable(condition: JoinEditor.() -> Unit) {
            if (isWriteQueryText) {
                val editor = JoinEditor().apply(condition)
                joinTable(editor.query.text)
            }
        }
    }
}

inline fun DeleteEditor.deleteFrom(
    table: String,
    set: DeleteEditor.Setter.() -> Unit = {}
): Statement {
    val setter = deleteSetter.apply(set)

    return deleteFrom(table, setter.query, setter.joinCondition)
}

inline fun <reified T : Entity> DeleteEditor.deleteFrom(
    set: DeleteEditor.Setter.() -> Unit = {}
): Statement {
    return deleteFrom(T::class.tableName, set)
}

inline fun <T : Entity> DeleteEditor.deleteFrom(
    table: String,
    elements: List<T>,
    set: DeleteEditor.Setter.(element: T) -> Unit
): Statement {
    val setter = deleteSetter.apply {
        set(elements.first())
        isWriteQueryText = false

        for (i in 1..elements.lastIndex) {
            set(elements[i])
        }
    }

    return deleteFrom(table, setter.query, setter.joinCondition)
}

inline fun <reified T : Entity> DeleteEditor.deleteFrom(
    elements: List<T>,
    set: DeleteEditor.Setter.(element: T) -> Unit
) =
    deleteFrom(T::class.tableName, elements, set)