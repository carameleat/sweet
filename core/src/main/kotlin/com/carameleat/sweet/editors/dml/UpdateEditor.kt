/*
 * Copyright 2021 Caramel Eat
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

//                              Bismillahirrahmanirrahim


package com.carameleat.sweet.editors.dml

import com.carameleat.sweet.libs.FieldSetter
import com.carameleat.sweet.editors.HighEditor
import com.carameleat.sweet.editors.JoinEditor
import com.carameleat.sweet.libs.*
import com.carameleat.sweet.libs.exts.tableName
import com.carameleat.sweet.libs.entity.Entity

interface UpdateEditor {

    val updateSetter: Setter get() = Setter()

    fun update(
        table: String,
        updateSetting: FieldSetting,
        condition: Query,
        joinCondition: String? = null
    ): Statement

    open class Setter : HighEditor() {

        @PublishedApi
        internal var fieldSetter = FieldSetter()
        val fieldSetting: FieldSetting get() = fieldSetter.setting
        var joinCondition: String? = null
            private set
        override var isWriteQueryText: Boolean
            get() = super.isWriteQueryText
            set(value) {
                super.isWriteQueryText = value
                fieldSetter.isAddFieldSet = value
            }

        fun joinTable(condition: String) {
            joinCondition = condition
        }

        inline fun joinTable(condition: JoinEditor.() -> Unit) {
            val editor = JoinEditor().apply(condition)
            joinTable(editor.query.text)
        }

        inline fun setFields(setting: FieldSetter.() -> Unit) {
            fieldSetter.apply(setting)
        }
    }
}

inline fun UpdateEditor.update(
    table: String,
    setting: UpdateEditor.Setter.() -> Unit
): Statement {
    val setter = updateSetter.apply(setting)

    return update(table, setter.fieldSetting, setter.query, setter.joinCondition)
}

inline fun <reified T : Entity> UpdateEditor.update(
    setting: UpdateEditor.Setter.() -> Unit
): Statement {
    return update(T::class.tableName, setting)
}

inline fun <T : Entity> UpdateEditor.update(
    table: String,
    elements: List<T>,
    setting: UpdateEditor.Setter.(element: T) -> Unit
): Statement {
    val setter = updateSetter.apply {
        setting(elements.first())
        isWriteQueryText = false

        for (i in 1..elements.lastIndex) {
            setting(elements[i])
        }
    }

    return update(table, setter.fieldSetting, setter.query, setter.joinCondition)
}

inline fun <reified T : Entity> UpdateEditor.update(
    elements: List<T>,
    setting: UpdateEditor.Setter.(value: T) -> Unit
): Statement = update(T::class.tableName, elements, setting)