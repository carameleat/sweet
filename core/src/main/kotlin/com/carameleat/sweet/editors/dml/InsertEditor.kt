/*
 * Copyright 2021 Caramel Eat
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

//                              Bismillahirrahmanirrahim


package com.carameleat.sweet.editors.dml

import com.carameleat.sweet.libs.FieldSetter
import com.carameleat.sweet.enums.ConflictStrategy
import com.carameleat.sweet.libs.*
import com.carameleat.sweet.libs.exts.tableName
import com.carameleat.sweet.libs.entity.Entity

interface InsertEditor {

    fun insertInto(
        table: String,
        conflictStrategy: ConflictStrategy?,
        insertSetting: FieldSetting,
        updateSetting: FieldSetting? = null
    ): Statement

    class Setter {

        @PublishedApi
        internal val insertSetter = FieldSetter()
        @PublishedApi
        internal val updateSetter = FieldSetter()
        val insertSetting: FieldSetting
            get() = insertSetter.setting
        val updateSetting: FieldSetting
            get() = updateSetter.setting
        var isWriteQueryText: Boolean = true
            set(value) {
                insertSetter.isAddFieldSet = value
                updateSetter.isAddFieldSet = value
                field = value
            }

        inline fun setFields(setting: FieldSetter.() -> Unit) {
            insertSetter.apply(setting)
        }

        inline fun updateStrategy(strategy: FieldSetter.() -> Unit) {
            updateSetter.apply(strategy)
        }
    }
}

inline fun InsertEditor.insertInto(
    table: String,
    conflictStrategy: ConflictStrategy? = null,
    setting: InsertEditor.Setter.() -> Unit
): Statement {
    val setter = InsertEditor.Setter().apply(setting)
    val insertSetting = setter.insertSetting
    val updateSetting = setter.updateSetting

    return insertInto(table, conflictStrategy, insertSetting, updateSetting)
}

inline fun <T : Entity> InsertEditor.insertInto(
    table: String,
    elements: List<T>,
    conflictStrategy: ConflictStrategy? = null,
    setting: InsertEditor.Setter.(element: T) -> Unit
): Statement {
    val setter = InsertEditor.Setter().apply {
        setting(elements.first())
        isWriteQueryText = false

        for (i in 1..elements.lastIndex) {
            setting(elements[i])
        }
    }
    val insertSetting = setter.insertSetting
    val updateSetting = setter.updateSetting

    return insertInto(table, conflictStrategy, insertSetting, updateSetting)
}

inline fun <reified T : Entity> InsertEditor.insertInto(
    conflictStrategy: ConflictStrategy? = null,
    setting: InsertEditor.Setter.() -> Unit
): Statement {
    return insertInto(T::class.tableName, conflictStrategy, setting)
}

inline fun <reified T : Entity> InsertEditor.insertInto(
    elements: List<T>,
    conflictStrategy: ConflictStrategy? = null,
    setting: InsertEditor.Setter.(material: T) -> Unit
): Statement = insertInto(T::class.tableName, elements, conflictStrategy, setting)