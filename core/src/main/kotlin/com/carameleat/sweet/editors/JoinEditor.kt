/*
 * Copyright 2021 Caramel Eat
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

//                              Bismillahirrahmanirrahim


package com.carameleat.sweet.editors

import com.carameleat.sweet.libs.Query
import com.carameleat.sweet.libs.Statement
import com.carameleat.sweet.libs.appends
import com.carameleat.sweet.libs.exts.tableColumnName
import com.carameleat.sweet.libs.exts.tableName
import com.carameleat.sweet.libs.exts.toQuery
import com.carameleat.sweet.libs.entity.Entity
import kotlin.reflect.KProperty1

class JoinEditor : LowEditor() {

    @PublishedApi
    internal val connector = Connector()
    override val query: Query
        get() = queryBuilder.toString().toQuery()

    /**
     * @param query your table name.
     */
    fun innerJoin(query: String) {
        write("INNER JOIN $query")
    }

    inline fun innerJoin(table: String, condition: Connector.() -> Unit) {
        innerJoin(table)
        connector.condition()
    }

    inline fun <reified T : Entity> innerJoin(condition: Connector.() -> Unit) {
        innerJoin(T::class.tableName, condition)
    }

    /**
     * @param query your table name.
     */
    fun rightJoin(query: String) {
        write("RIGHT JOIN $query")
    }

    inline fun rightJoin(table: String, condition: Connector.() -> Unit) {
        rightJoin(table)
        connector.condition()
    }

    inline fun <reified T : Entity> rightJoin(condition: Connector.() -> Unit) {
        rightJoin(T::class.tableName, condition)
    }

    /**
     * @param query your table name.
     */
    fun leftJoin(query: String) {
        write("LEFT JOIN $query")
    }

    inline fun leftJoin(table: String, condition: Connector.() -> Unit) {
        leftJoin(table)
        connector.condition()
    }

    inline fun <reified T : Entity> leftJoin(condition: Connector.() -> Unit) {
        leftJoin(T::class.tableName, condition)
    }

    /**
     * @param query your table name.
     */
    fun fullJoin(query: String) {
        queryBuilder.appends("FULL OUTER JOIN $query")
    }

    inline fun fullJoin(table: String, condition: Connector.() -> Unit) {
        fullJoin(table)
        connector.condition()
    }

    inline fun <reified T : Entity> fullJoin(condition: Connector.() -> Unit) {
        fullJoin(T::class.tableName, condition)
    }

    inner class Connector {

        /**
         * @param anotherColumn your related column.
         */
        infix fun String.isEquals(anotherColumn: String) {
            write("ON $this = $anotherColumn")
        }

        /**
         * @param anotherColumn [KProperty1],  which representing your related column.
         * Does not compatible with [KProperty1],  from another model class
         * which is not the base table in the [Statement].
         */
        inline infix fun <reified T : Entity> String.isEquals(
            anotherColumn: KProperty1<T, *>
        ) {
            this isEquals anotherColumn.tableColumnName
        }

        /**
         * This extension method is not compatible with [KProperty1]T,  from
         * another model class which is not the base table in the [Statement].
         *
         * @param anotherColumn your related column.
         */
        inline infix fun <reified T : Entity> KProperty1<T, *>.isEquals(
            anotherColumn: String
        ) {
            tableColumnName isEquals anotherColumn
        }

        inline infix fun <reified T : Entity, reified E : Entity> KProperty1<T, *>.isEquals(
            anotherColumn: KProperty1<E, *>
        ) {
            tableColumnName isEquals anotherColumn.tableColumnName
        }
    }
}