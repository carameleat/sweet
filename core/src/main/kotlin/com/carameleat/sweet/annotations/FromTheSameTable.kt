/*
 * Copyright 2021 Caramel Eat
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

//                                  Bismillahirrahmanirrahim


package com.carameleat.sweet.annotations

/**
 * Informing that the property is from the same table.
 * Example :
 * ```
 * create table persons(
 *  id int not null primary key auto_increment,
 *  name varchar(20) not null,
 *  city varchar(50) null,
 *  country varchar(50) null
 * );
 *
 * --------------------------------
 *
 * data class Address(
 *  val city: String,
 *  val country: String
 * )
 *
 * ---------------------------------
 *
 * data class Person(
 *  val id: Int,
 *  val name: String,
 *  @FromTheSameTable
 *  val address: Address,
 * )
 * ```
 */
@Target(AnnotationTarget.PROPERTY)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class FromTheSameTable
