/*
 * Copyright 2021 Caramel Eat
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

//                                  Bismillahirrahmanirrahim


package com.carameleat.sweet.annotations

/**
 * Informing that the property is from another table. Example :
 * ```
 * @Table("schools")
 * data class School(
 *  val id: Int = 0,
 *  val name: String,
 *  //...
 * )
 *
 * @Table("students")
 * data class Student(
 *  val id: Int = 0
 *  val name: String,
 *  val email: String,
 *  @FromAnotherTable
 *  val school: School
 * )
 * ```
 *
 * The annotated property will be ignored by Writers when creating INSERT, UPDATE, and DELETE statement, inshaallah.
 *
 */
@Target(AnnotationTarget.PROPERTY)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class FromAnotherTable