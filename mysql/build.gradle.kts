//                                  Bismillahirrahmanirrahim


import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm")
    kotlin("kapt")
    id("com.github.johnrengelman.shadow")
}

val group_name: String by project
val sweet_version: String by project
group = group_name
version = sweet_version

tasks.withType<KotlinCompile>().all {
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

//val allJavadoc = task("allJavadoc", Javadoc::class) {
//    source(kotlin.sourceSets.main.get().kotlin)
//    classpath = files(sourceSets.main.get().compileClasspath)
//    setDestinationDir(file("$buildDir/docs/javadoc"))
//}

val jarBaseName = "Sweet-MySQL"
tasks.jar {
    archiveBaseName.set(jarBaseName)
    archiveVersion.set(sweet_version)

    manifest {
        manifest {
            attributes(
                mapOf(
                    "Implementation-Title" to jarBaseName,
                    "Implementation-Version" to archiveVersion.get()
                )
            )
        }
    }

    from(
        configurations.runtimeClasspath.get().map {
            if (it.isDirectory) it else zipTree(it)
        },
        kotlin.sourceSets.main.get().kotlin,
        project(":core").kotlin.sourceSets.main.get().kotlin
    )
}

//val javadocJar = task("javadocJar", Jar::class) {
//    archiveBaseName.set(jarBaseName)
//    archiveClassifier.set("javadoc")
//    dependsOn(allJavadoc)
//    from(allJavadoc.destinationDir)
//}
//
//val sourceJar = task("sourceJar", Jar::class) {
//    archiveBaseName.set(jarBaseName)
//    archiveClassifier.set("sources")
//
//    from(kotlin.sourceSets.main.get().kotlin)
//}
//
//tasks.shadowJar {
//    archiveBaseName.set(jarBaseName)
//    manifest {
//        attributes(
//            mapOf(
//                "Implementation-Title" to "Sweet-MySQL",
//                "Implementation-Version" to archiveVersion.get()
//            )
//        )
//    }
//
//    finalizedBy(sourceJar)
//}

dependencies {
    implementation(project(":core"))

    testImplementation("junit", "junit", "4.13")
}
