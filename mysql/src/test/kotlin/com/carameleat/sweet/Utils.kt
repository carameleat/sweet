/*
 * Copyright 2021 Caramel Eat
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.carameleat.sweet

import com.carameleat.sweet.writers.Writer
import com.flife.sweet.mysql.writers.DefaultDeleteWriter
import com.flife.sweet.mysql.writers.DefaultInsertWriter
import com.flife.sweet.mysql.writers.DefaultSelectWriter
import com.flife.sweet.mysql.writers.DefaultUpdateWriter

fun initWriters() {
    Writer.apply {
        InsertWriter = DefaultInsertWriter()
        UpdateWriter = DefaultUpdateWriter()
        DeleteWriter = DefaultDeleteWriter()
        SelectWriter = DefaultSelectWriter()
    }
}