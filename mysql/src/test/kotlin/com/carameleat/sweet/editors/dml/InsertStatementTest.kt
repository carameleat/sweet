/*
 * Copyright 2021 Caramel Eat
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.carameleat.sweet.editors.dml

import com.carameleat.sweet.Tester
import com.carameleat.sweet.enums.ConflictStrategy
import com.carameleat.sweet.enums.Gender
import com.carameleat.sweet.libs.exts.tableName
import com.carameleat.sweet.models.Student
import com.carameleat.sweet.models.studentList
import com.carameleat.sweet.writers.Writer
import com.flife.sweet.mysql.editors.DMLEditor
import org.junit.Test

class InsertStatementTest : Tester(), DMLEditor {

    private val studentList: List<Student> = studentList(3)
    private val student = Student(1, "Fauzul", Gender.MALE)

    @Test
    fun test1() {
        val statement = Writer.writeInsert(student)
        println()
        println(statement.toString())
        println()
    }

    @Test
    fun test2() {
        val statement = Writer.writeInsert(student, ConflictStrategy.UPDATE)
        println()
        println(statement.toString())
        println()
    }

    @Test
    fun test3() {
        val statement = Writer.writeInsert(student, ConflictStrategy.REPLACE)
        println()
        println(statement.toString())
        println()
    }

    @Test
    fun test4() {
        val statement = Writer.writeInsert(student, ConflictStrategy.IGNORE)
        println()
        println(statement.toString())
        println()
    }

    @Test
    fun test5() {
        val statement = Writer.writeInsert(student) {
            Student::name to it.name
        }
        println()
        println(statement.toString())
        println()
    }

    @Test
    fun test6() {
        val statement = Writer.writeInsert(studentList)
        println()
        println(statement.batchesToString())
        println()
    }

    @Test
    fun test7() {
        val statement = Writer.writeInsert(studentList, ConflictStrategy.UPDATE)
        println()
        println(statement.batchesToString())
        println()
    }

    @Test
    fun test8() {
        val statement = Writer.writeInsert(studentList, ConflictStrategy.REPLACE)
        println()
        println(statement.batchesToString())
        println()
    }

    @Test
    fun test9() {
        val statement = Writer.writeInsert(studentList, ConflictStrategy.IGNORE)
        println()
        println(statement.batchesToString())
        println()
    }

    @Test
    fun test10() {
        val statement = Writer.writeInsert(studentList) { student ->
            Student::gender to student.gender
        }
        println()
        println(statement.batchesToString())
        println()
    }

    @Test
    fun test11() {
        val statement = insertInto(Student::class.tableName) {
            setFields {
                Student::name to student.name
                Student::gender to student.gender
            }
        }
        println()
        println(statement.toString())
        println()
    }

    @Test
    fun test13() {
        val statement = insertInto(Student::class.tableName, ConflictStrategy.UPDATE) {
            setFields {
                Student::name to student.name
                Student::gender to student.gender
            }

            updateStrategy {
                Student::name to student.name
            }
        }
        println()
        println(statement.toString())
        println()
    }

    @Test
    fun test14() {
        val statement = insertInto(Student::class.tableName, ConflictStrategy.REPLACE) {
            setFields {
                Student::name to student.name
                Student::gender to student.gender
            }
        }
        println()
        println(statement.toString())
        println()
    }

    @Test
    fun test15() {
        val statement = insertInto(Student::class.tableName, ConflictStrategy.IGNORE) {
            setFields {
                Student::name to student.name
                Student::gender to student.gender
            }
        }
        println()
        println(statement.toString())
        println()
    }

    @Test
    fun test16() {
        val statement = insertInto(Student::class.tableName, studentList) { student ->
            setFields {
                Student::name to student.name
                Student::gender to student.gender
            }
        }
        println()
        println(statement.batchesToString())
        println()
    }

    @Test
    fun test18() {
        val statement = insertInto(Student::class.tableName, studentList, ConflictStrategy.UPDATE) { student ->
            setFields {
                Student::name to student.name
                Student::gender to student.gender
            }

            updateStrategy {
                Student::name to student.name
            }
        }
        println()
        println(statement.batchesToString())
        println()
    }

    @Test
    fun test19() {
        val statement = insertInto(Student::class.tableName, studentList, ConflictStrategy.REPLACE) { student ->
            setFields {
                Student::name to student.name
                Student::gender to student.gender
            }
        }
        println()
        println(statement.batchesToString())
        println()
    }

    @Test
    fun test20() {
        val statement = insertInto(Student::class.tableName, studentList, ConflictStrategy.IGNORE) { student ->
            setFields {
                Student::name to student.name
                Student::gender to student.gender
            }
        }
        println()
        println(statement.batchesToString())
        println()
    }

    @Test
    fun test21() {
        val statement = insertInto<Student> {
            setFields {
                Student::name to student.name
                Student::gender to student.gender
            }
        }
        println()
        println(statement.toString())
        println()
    }

    @Test
    fun test22() {
        val statement = insertInto<Student>(ConflictStrategy.UPDATE) {
            setFields {
                Student::name to student.name
                Student::gender to student.gender
            }

            updateStrategy {
                Student::name to student.name
            }
        }
        println()
        println(statement.toString())
        println()
    }

    @Test
    fun test23() {
        val statement = insertInto<Student>(ConflictStrategy.REPLACE) {
            setFields {
                Student::name to student.name
                Student::gender to student.gender
            }
        }
        println()
        println(statement.toString())
        println()
    }

    @Test
    fun test24() {
        val statement = insertInto<Student>(ConflictStrategy.IGNORE) {
            setFields {
                Student::name to student.name
                Student::gender to student.gender
            }
        }
        println()
        println(statement.toString())
        println()
    }

    @Test
    fun test25() {
        val statement = insertInto(studentList) { student ->
            setFields {
                Student::name to student.name
                Student::gender to student.gender
            }
        }
        println()
        println(statement.batchesToString())
        println()
    }

    @Test
    fun test26() {
        val statement = insertInto(studentList) { student ->
            setFields {
                Student::name to student.name
                Student::gender to student.gender
            }
        }
        println()
        println(statement.batchesToString())
        println()
    }

    @Test
    fun test27() {
        val statement = insertInto(studentList, ConflictStrategy.UPDATE) { student ->
            setFields {
                Student::name to student.name
                Student::gender to student.gender
            }

            updateStrategy {
                Student::name to student.name
            }
        }
        println()
        println(statement.batchesToString())
        println()
    }

    @Test
    fun test28() {
        val statement = insertInto(studentList, ConflictStrategy.REPLACE) { student ->
            setFields {
                Student::name to student.name
                Student::gender to student.gender
            }
        }
        println()
        println(statement.batchesToString())
        println()
    }

    @Test
    fun test29() {
        val statement = insertInto(studentList, ConflictStrategy.IGNORE) { student ->
            setFields {
                Student::name to student.name
                Student::gender to student.gender
            }
        }
        println()
        println(statement.batchesToString())
        println()
    }
}