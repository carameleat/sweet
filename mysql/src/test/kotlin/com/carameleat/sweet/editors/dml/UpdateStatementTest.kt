/*
 * Copyright 2021 Caramel Eat
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.carameleat.sweet.editors.dml

import com.carameleat.sweet.Tester
import com.carameleat.sweet.enums.Gender
import com.carameleat.sweet.libs.exts.tableName
import com.carameleat.sweet.writers.Writer
import com.carameleat.sweet.models.Student
import com.carameleat.sweet.models.studentList
import com.flife.sweet.mysql.editors.DMLEditor
import org.junit.Test

class UpdateStatementTest : Tester(), DMLEditor {

    private val studentList = studentList(3)
    private val student = Student(1, "Fauzul", Gender.MALE)

    @Test
    fun test1() {
        val statement = Writer.writeUpdate(student) {
            where {
                Student::id equalTo it.id
            }
        }
        println()
        println(statement.toString())
        println()
    }

    @Test
    fun test2() {
        val statement = Writer.writeUpdate(studentList) { student ->
            where {
                Student::id equalTo student.id
            }
        }
        println(statement.parameter.batchSize)
        println(statement.parameter.batchCount)
        println(statement.parameter.values.joinToString())
        println()
        println(statement.batchesToString())
        println()
    }

    @Test
    fun test3() {
        val statement = update(Student::class.tableName) {
            setFields {
                Student::name to student.name
                Student::gender to student.gender
            }

            where {
                Student::id equalTo student.id
            }
        }
        println()
        println(statement.toString())
        println()
    }

    @Test
    fun test4() {
        val statement = update(Student::class.tableName, studentList) { student ->
            setFields {
                Student::name to student.name
                Student::gender to student.gender
            }

            where {
                Student::id equalTo student.id
            }
        }
        println()
        println(statement.batchesToString())
        println()
    }

    @Test
    fun test5() {
        val statement = update<Student> {
            setFields {
                Student::name to student.name
                Student::gender to student.gender
            }

            where {
                Student::id equalTo student.id
            }
        }
        println()
        println(statement.toString())
        println()
    }

    @Test
    fun test6() {
        val statement = update(studentList) { student ->
            setFields {
                Student::name to student.name
                Student::gender to student.gender
            }

            where {
                Student::id equalTo student.id
            }
        }
        println()
        println(statement.batchesToString())
        println()
    }
}