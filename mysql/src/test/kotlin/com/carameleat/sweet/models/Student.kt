/*
 * Copyright 2021 Caramel Eat
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.carameleat.sweet.models

import com.carameleat.sweet.annotations.Ignore
import com.carameleat.sweet.annotations.Table
import com.carameleat.sweet.enums.Gender
import com.carameleat.sweet.enums.Operation
import com.carameleat.sweet.libs.entity.Entity
import kotlin.random.Random

@Table("students")
data class Student(
    @Ignore(Operation.INSERT, Operation.UPDATE)
    /*
    Inshaallah will be ignored on .toInsertStatement() and .toUpdateStatement() extensions
    that inshallah will generate the SQL query.
     */
    val id: Int,
    val name: String,
    val gender: String
) : Entity {

    constructor(
        id: Int,
        name: String,
        gender: Gender
    ) : this(id, name, gender.toString())
}

fun studentList(size: Int = -1): List<Student> {
    if (size == 0) return emptyList()

    val times = if (size != -1) size else Random.nextInt(1, 20)
    val students = mutableListOf<Student>()
    repeat(times) {
        val student = Student(1, "Fauzul", Gender.MALE)
        students.add(student)
    }

    return students
}