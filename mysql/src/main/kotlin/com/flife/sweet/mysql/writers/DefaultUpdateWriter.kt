/*
 * Copyright 2021 Caramel Eat
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.flife.sweet.mysql.writers

import com.carameleat.sweet.enums.Operation
import com.carameleat.sweet.libs.FieldSetting
import com.carameleat.sweet.libs.Query
import com.carameleat.sweet.libs.Statement
import com.carameleat.sweet.libs.entity.Entity
import com.carameleat.sweet.libs.entity.findFieldSetting
import com.carameleat.sweet.libs.exts.tableName
import com.carameleat.sweet.writers.UpdateWriter

open class DefaultUpdateWriter : UpdateWriter {

    override fun <T : Entity> write(value: T, condition: Query): Statement {
        val table = value::class.tableName

        return writeTo(Statement(), table, findSettingOf(value), condition)
    }

    override fun <T : Entity> write(values: List<T>, condition: Query): Statement {
        val table = values.first()::class.tableName

        return writeTo(Statement(), table, findSettingOf(values), condition)
    }

    override fun writeTo(
        statement: Statement,
        table: String,
        setting: FieldSetting,
        condition: Query
    ): Statement {
        return statement.apply {
            write("UPDATE $table SET")

            setting.let {
                for (index in 0 until it.sets.lastIndex) {
                    val set = it.sets[index]
                    write("${set.field} = ${set.parameterText},")
                }
                val set = it.sets.last()
                write("${set.field} = ${set.parameterText}")

                parameter.add(it.parameter)
            }

            write(condition)
        }
    }

    private fun <T : Entity> findSettingOf(value: T): FieldSetting =
        value.findFieldSetting(Operation.UPDATE)

    private fun <T : Entity> findSettingOf(
        values: List<T>
    ): FieldSetting = values.findFieldSetting(Operation.UPDATE)
}