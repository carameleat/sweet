/*
 * Copyright 2021 Caramel Eat
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.flife.sweet.mysql.editors

import com.carameleat.sweet.editors.dml.DeleteEditor
import com.carameleat.sweet.editors.dml.InsertEditor
import com.carameleat.sweet.editors.dml.SelectEditor
import com.carameleat.sweet.editors.dml.UpdateEditor
import com.carameleat.sweet.enums.ConflictStrategy
import com.carameleat.sweet.libs.FieldSetting
import com.carameleat.sweet.libs.Query
import com.carameleat.sweet.libs.SelectComponent
import com.carameleat.sweet.libs.Statement
import com.carameleat.sweet.writers.Writer

interface DMLEditor : InsertEditor, UpdateEditor, DeleteEditor, SelectEditor {

    override fun insertInto(
        table: String,
        conflictStrategy: ConflictStrategy?,
        insertSetting: FieldSetting,
        updateSetting: FieldSetting?
    ): Statement {
        val statement = Statement()
        with(Writer.InsertWriter) {
            when (conflictStrategy) {
                ConflictStrategy.UPDATE -> {
                    writeUsingUpdateStrategyTo(statement, table, insertSetting, updateSetting)
                }
                ConflictStrategy.REPLACE -> {
                    writeUsingReplaceStrategyTo(statement, table, insertSetting)
                }
                ConflictStrategy.IGNORE -> {
                    writeUsingIgnoreStrategyTo(statement, table, insertSetting)
                }
                else -> writeUsingNoStrategyTo(statement, table, insertSetting)
            }
        }

        return statement
    }

    override fun update(
        table: String,
        updateSetting: FieldSetting,
        condition: Query,
        joinCondition: String?
    ): Statement {
        val mTable = if (joinCondition == null) table else "$table $joinCondition"

        return Writer.UpdateWriter.writeTo(Statement(), mTable, updateSetting, condition)
    }

    override fun deleteFrom(table: String, condition: Query, joinCondition: String?): Statement {
        val mTable = if (joinCondition == null) table else "$table $joinCondition"
        
        return Writer.DeleteWriter.writeTo(Statement(), mTable, condition)
    }

    override fun select(component: SelectComponent, condition: Query): Statement {
        return Writer.SelectWriter.writeTo(Statement(), component, condition)
    }
}