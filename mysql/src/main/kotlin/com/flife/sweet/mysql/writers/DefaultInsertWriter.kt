/*
 * Copyright 2021 Caramel Eat
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.flife.sweet.mysql.writers

import com.carameleat.sweet.enums.ConflictStrategy
import com.carameleat.sweet.enums.Operation
import com.carameleat.sweet.libs.FieldSetting
import com.carameleat.sweet.libs.Statement
import com.carameleat.sweet.libs.entity.Entity
import com.carameleat.sweet.libs.entity.findFieldSetting
import com.carameleat.sweet.libs.exts.tableName
import com.carameleat.sweet.writers.InsertWriter

open class DefaultInsertWriter : InsertWriter {

    override fun <T : Entity> write(value: T, conflictStrategy: ConflictStrategy?): Statement {
        val table = value::class.tableName
        val setting = findSettingOf(value)
        val statement = Statement()

        return when (conflictStrategy) {
            ConflictStrategy.UPDATE -> writeUsingUpdateStrategyTo(statement, table, setting)
            ConflictStrategy.REPLACE -> writeUsingReplaceStrategyTo(statement, table, setting)
            ConflictStrategy.IGNORE -> writeUsingIgnoreStrategyTo(statement, table, setting)
            else -> writeUsingNoStrategyTo(statement, table, setting)
        }
    }

    override fun <T : Entity> write(values: List<T>, conflictStrategy: ConflictStrategy?): Statement {
        val table = values.first()::class.tableName
        val setting = findSettingOf(values)
        val statement = Statement()

        return when (conflictStrategy) {
            ConflictStrategy.UPDATE -> writeUsingUpdateStrategyTo(statement, table, setting)
            ConflictStrategy.REPLACE -> writeUsingReplaceStrategyTo(statement, table, setting)
            ConflictStrategy.IGNORE -> writeUsingIgnoreStrategyTo(statement, table, setting)
            else -> writeUsingNoStrategyTo(statement, table, setting)
        }
    }

    override fun <T : Entity> write(value: T, updateStrategy: FieldSetting?): Statement {
        val table = value::class.tableName
        val setting = findSettingOf(value)

        return writeUsingUpdateStrategyTo(Statement(), table, setting, updateStrategy)
    }

    override fun <T : Entity> write(values: List<T>, updateStrategy: FieldSetting?): Statement {
        val table = values.first()::class.tableName
        val setting = findSettingOf(values)
        val statement = Statement()

        return writeUsingUpdateStrategyTo(statement, table, setting, updateStrategy)
    }

    override fun writeUsingUpdateStrategyTo(
        statement: Statement,
        table: String,
        insertSetting: FieldSetting,
        updateSetting: FieldSetting?
    ): Statement {
        return statement.apply {
            write("INSERT INTO $table")
            writeColumns(insertSetting)
            write("ON DUPLICATE KEY UPDATE")

            if (updateSetting != null && updateSetting.isNotEmpty()) {
                updateSetting.let {
                    for (i in 0 until it.sets.lastIndex) {
                        val set = it.sets[i]
                        write("${set.field} = ${set.parameterText},")
                    }

                    val set = it.sets.last()
                    write("${set.field} = ${set.parameterText}")

                    parameter.add(it.parameter)
                }
            } else {
                val sets = insertSetting.sets
                for (i in 0 until sets.lastIndex) {
                    val column = sets[i].field
                    write("$column = VALUES($column),")
                }

                val column = sets.last().field
                write("$column = VALUES($column)")
            }
        }
    }

    override fun writeUsingReplaceStrategyTo(
        statement: Statement,
        table: String,
        setting: FieldSetting
    ): Statement {
        return statement.apply {
            write("REPLACE INTO $table")
            writeColumns(setting)
        }
    }

    override fun writeUsingIgnoreStrategyTo(
        statement: Statement,
        table: String,
        setting: FieldSetting
    ): Statement {
        return statement.apply {
            write("INSERT IGNORE INTO $table")
            writeColumns(setting)
        }
    }

    override fun writeUsingNoStrategyTo(statement: Statement, table: String, setting: FieldSetting): Statement {
        return statement.apply {
            write("INSERT INTO $table")
            writeColumns(setting)
        }
    }

    private fun Statement.writeColumns(setting: FieldSetting) {
        val columnBuilder = StringBuilder().apply {
            append("(")
        }
        val valuesBuilder = StringBuilder().apply {
            append("VALUES (")
        }

        setting.let {
            for (index in 0 until it.sets.lastIndex) {
                val set = it.sets[index]
                columnBuilder.append(" ${set.field},")
                valuesBuilder.append(" ${set.parameterText},")
            }
            val set = it.sets.last()
            columnBuilder.append(" ${set.field} )")
            valuesBuilder.append(" ${set.parameterText} )")

            write("$columnBuilder $valuesBuilder", it.parameter)
        }
    }

    /**
     * Find the column(s) and add it value to the [statement].values.
     *
     * @param value the value which want to be inserted.
     * @return list of column(s) in [String]. Make sure to write or append it
     * on [statement].syntax sequentially from index to 0 to the last
     * or the value can be reversed.
     */
    private fun <T : Entity> findSettingOf(
        value: T
    ) = value.findFieldSetting(Operation.INSERT)

    /**
     * Find the column(s) and add it value to the [statement].values.
     *
     * @param values list of values which want to be inserted.
     * @return list of column(s) in [String]. Make sure to write or append it
     * on [statement].syntax sequentially from index to 0 to the last
     * or the value can be reversed.
     */
    private fun <T : Entity> findSettingOf(
        values: List<T>
    ) = values.findFieldSetting(Operation.INSERT)
}